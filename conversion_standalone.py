from __future__ import division
import glob
import os
import math
import time
import tqdm
import ast
# import torch
# import torch.nn as nn
# import torch.nn.functional as F
# from torch.autograd import Variable
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as patches
import shutil

def read_convert_file(convert_file='class.convertion'):

    with open(convert_file) as f:
        old_labels =  []
        new_labels = []
        for line in f:
            old_label = line.split(' ')[0]
            old_labels.append(old_label)
            new_label = line.split(' ')[1][:-1] # remove \n
            new_labels.append(new_label)
    
    return old_labels, new_labels

def remove_some_files(dir,extension = '_convert.txt'):
    
    files_to_rn = [f for f in os.listdir(dir) if f.endswith('.txt')]

    for file in files_to_rn :
    
        if not file.endswith(extension) :
            # print(file)
            full_name = dir + '/' + file
            os.remove(full_name)



def rename_some_files(dir_to_rn,old_ext='_new.txt', new_ext='.txt'):

    files_to_rn = [f for f in os.listdir(dir_to_rn) if f.endswith(old_ext)]
    # print(files_to_rn)
    len_ext = len(old_ext)

    # changement d'extension
    for file in files_to_rn:
        name_with_path = dir_to_rn + '/' + file
        base = name_with_path[:-len_ext]
        new_name_with_path = base + new_ext
        # print(new_name_with_path)
        os.rename(name_with_path, new_name_with_path)
    



def convert_al_labels(dir_to_convert='data/train/original',convert_file='data/class.convertion'):
    
    # read convertion
    old_labels, new_labels = read_convert_file(convert_file=convert_file)
    print(old_labels)
    print(new_labels)
    
    # list txt files
    txt_labels = sorted([f for f in os.listdir(dir_to_convert) if f.endswith('.txt')])

    # change first char of every line of every file according to labels
    for file in txt_labels :
        
        # print(file)
        
        full_name = os.path.join(dir_to_convert, file)
        full_name_no_ext = full_name[:-4]
        outname = full_name_no_ext + '_convert.txt' # new_labels are named *_convert.txt
        new_label = open(outname,"w")

        with open(full_name) as f:
            for line in f:
                line = "".join(line)
                if line.split(' ')[0] in old_labels :
                    print('pouet')
                    i = old_labels.index(line.split(' ')[0]) # very ugly i should have gone for a dict but it works
                    num_digits = len(line.split(' ')[0])
                    line = list(line)
                    line[:num_digits] = new_labels[i]
                    line_str = "".join(line)
                # print(line_str)
                new_label.write(line_str)
                # print(line)
        new_label.close()

    # remove all .txt files except _convert.txt
    remove_some_files(dir_to_convert)
    rename_some_files(dir_to_convert,old_ext='_convert.txt',new_ext='.txt')

def convert_al_labels_fix(dir_to_convert='data/train/original',convert_file='data/class.convertion'):
    
    # read convertion
    old_labels, new_labels = read_convert_file(convert_file=convert_file)
    print(old_labels)
    print(new_labels)
    
    # list txt files
    txt_labels = sorted([f for f in os.listdir(dir_to_convert) if (f.endswith('.txt') and f.startswith('Cheilomenes_sulphurea'))])
    print(txt_labels)

    # change first char of every line of every file according to labels
    for file in txt_labels :
        
        # print(file)
        
        full_name = os.path.join(dir_to_convert, file)
        full_name_no_ext = full_name[:-4]
        outname = full_name_no_ext + '_convert.txt' # new_labels are named *_convert.txt
        new_label = open(outname,"w")

        with open(full_name) as f:
            for line in f:
                line = "".join(line)
                if line.split(' ')[0] == '' :
                    print('pouet')
                    i = old_labels.index(line.split(' ')[0]) # very ugly i should have gone for a dict but it works
                    num_digits = len(line.split(' ')[0])
                    line = list(line)
                    line = [new_labels[i]] + line
                    line_str = "".join(line)
                # print(line_str)
                new_label.write(line_str)
                # print(line)
        new_label.close()

    # remove all .txt files except _convert.txt
    remove_some_files(dir_to_convert)
    rename_some_files(dir_to_convert,old_ext='_convert.txt',new_ext='.txt')

def check_labels(dir_to_check='Lycosidae/Lycosidae_msp1_shooting_1'):

    labels = sorted([f[:-4] for f in os.listdir(dir_to_check) if f.endswith('.txt')])
    images = sorted([f[:-4] for f in os.listdir(dir_to_check) if f.endswith('.JPG')])

    # print(labels)
    # print(images)

    label_problems = []
    images_problems = []

    for label in labels:
        if label not in images:
            label_problems.append(label)

    for image in images:
        if image not in labels:
            images_problems.append(image)

    if len(label_problems) != 0:
        print('suppelementary labels:')
        print(label_problems)
    

    if len(images_problems) != 0:
        print('suppelementary images:')
        print(images_problems)
    
    if (len(images_problems) == 0) and (len(label_problems) == 0):
        print("tout bon")





convert_al_labels('data/test/original/paradoxo_temp')
# convert_al_labels('data/train/sliced/labels')



# all_dirs = [x[0] for x in os.walk('data/test/sliced/labels')]

# for dir in all_dirs:

#     files_to_list = [f for f in os.listdir(dir) if f.endswith('.txt')]
#     classes_list = []
#     check_labels(dir)

#     for label_file in files_to_list:
#         path = dir + "/" + label_file
#         with open(path) as f :
#             for line in f:
#                 class_num = line.split(' ')[0]
#                 if class_num == '':
#                     print(path)
#                 classes_list.append(class_num)

#                 # if class_num == '2':
#                 #     print(path)
    
#     if len(set(classes_list)) != 0:
#         print(dir)
#         print(set(classes_list))

