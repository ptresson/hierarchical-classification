from __future__ import division
from __future__ import print_function 

from models import *
#from utils.logger import *
from utils.utils import *
from utils.datasets import *
from utils.parse_config import *
from test import evaluate, test_classifier

from terminaltables import AsciiTable

import os
import sys
import time
import datetime
import argparse
import psutil
import copy
import numpy as np
# import matplotlib.pyplot as plt

import torch
import torchvision
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
from torchvision import datasets
from torchvision import transforms
from torch.autograd import Variable
import torch.optim as optim
import torchvision.models as models
from torch.utils.tensorboard import SummaryWriter


# print("PyTorch Version: ",torch.__version__)
# print("Torchvision Version: ",torchvision.__version__)



def train_detector(epochs=100, batch_size=4, gradient_accumulations=2, model_def="config/yolov3-custom.cfg", 
data_config="config/train.data", pretrained_weights="weights/darknet53.conv.74" , n_cpu=12, 
img_size=416, checkpoint_interval=1, evaluation_interval=1, compute_map=False, 
multiscale_training=True, check_hardware=True):
    """
    Train the detector. Code based on https://github.com/eriklindernoren/PyTorch-YOLOv3
    with minor changes concerning tensorboard log and pauses during training if high temperatures.

    """


    ##################### for tensorboard #######################
    writer = SummaryWriter()
    #################################################################

    # use GPU if available
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    os.makedirs("output", exist_ok=True)
    os.makedirs("checkpoints", exist_ok=True)

    # Get data configuration
    data_config = parse_data_config(data_config)
    train_path = data_config["train"]
    valid_path = data_config["valid"]
    class_names = load_classes(data_config["names"])

    # Initiate model
    model = Darknet(model_def).to(device)
    model.apply(weights_init_normal)

    # If specified we start from checkpoint
    if pretrained_weights:
        if pretrained_weights.endswith(".pth"):
            model.load_state_dict(torch.load(pretrained_weights))
        else:
            model.load_darknet_weights(pretrained_weights)

    # Get dataloader
    dataset = ListDataset(train_path, augment=True, multiscale=multiscale_training)
    dataloader = torch.utils.data.DataLoader(
        dataset,
        batch_size=batch_size,
        shuffle=True,
        num_workers=n_cpu,
        pin_memory=True,
        collate_fn=dataset.collate_fn,
    )

    optimizer = torch.optim.Adam(model.parameters())

    metrics = [
        "grid_size",
        "loss",
        "x",
        "y",
        "w",
        "h",
        "conf",
        "cls",
        "cls_acc",
        "recall50",
        "recall75",
        "precision",
        "conf_obj",
        "conf_noobj",
    ]
    
    # actual training starts

    for epoch in range(epochs):
        model.train()
        start_time = time.time()
        for batch_i, (_, imgs, targets) in enumerate(dataloader):
            batches_done = len(dataloader) * epoch + batch_i

            imgs = Variable(imgs.to(device))
            targets = Variable(targets.to(device), requires_grad=False)

            loss, outputs = model(imgs, targets)
            loss.backward()

            if batches_done % gradient_accumulations:
                # Accumulates gradient before each step
                optimizer.step()
                optimizer.zero_grad()

            # ----------------
            #   Log progress
            # ----------------

            log_str = "\n---- [Epoch %d/%d, Batch %d/%d] ----\n" % (epoch, epochs, batch_i, len(dataloader))

            metric_table = [["Metrics", *[f"YOLO Layer {i}" for i in range(len(model.yolo_layers))]]]

            # Log metrics at each YOLO layer
            for i, metric in enumerate(metrics):
                formats = {m: "%.6f" for m in metrics}
                formats["grid_size"] = "%2d"
                formats["cls_acc"] = "%.2f%%"
                row_metrics = [formats[metric] % yolo.metrics.get(metric, 0) for yolo in model.yolo_layers]
                metric_table += [[metric, *row_metrics]]


            log_str += AsciiTable(metric_table).table
            log_str += f"\nTotal loss {loss.item()}"

            # Determine approximate time left for epoch
            epoch_batches_left = len(dataloader) - (batch_i + 1)
            time_left = datetime.timedelta(seconds=epoch_batches_left * (time.time() - start_time) / (batch_i + 1))
            log_str += f"\n---- ETA {time_left}"

            print(log_str)

            model.seen += imgs.size(0)
        

        ##################### tensorboard loss ######################
        writer.add_scalar('Loss', loss.item(), epoch)
        #############################################################

        # evaluate model 
        if epoch % evaluation_interval == 0:
            print("\n---- Evaluating Model ----")
            # Evaluate the model on the validation set
            precision, recall, AP, f1, ap_class = evaluate(
                model,
                path=valid_path,
                iou_thres=0.5,
                conf_thres=0.5,
                nms_thres=0.5,
                img_size=img_size,
                batch_size=4,
            )
            evaluation_metrics = [
                ("val_precision", precision.mean()),
                ("val_recall", recall.mean()),
                ("val_mAP", AP.mean()),
                ("val_f1", f1.mean()),
            ]

            # Print class APs and mAP
            ap_table = [["Index", "Class name", "AP"]]
            for i, c in enumerate(ap_class):
                ap_table += [[c, class_names[c], "%.5f" % AP[i]]]
            print(AsciiTable(ap_table).table)
            print(f"---- mAP {AP.mean()}")

            ################### tensorboard metrics #####################
            writer.add_scalar('Perf/f1', float(f1.mean()), epoch)
            writer.add_scalar('Perf/mAP', float(AP.mean()), epoch)
            ############################################################

        # save model
        if epoch % checkpoint_interval == 0:
            torch.save(model.state_dict(), f"checkpoints/yolov3_ckpt_%d.pth" % epoch)

            # taking the opportunity to check hardware state
            if check_hardware: # checks CPU and not GPU but it is more to get a general idea of the hardware state
                if float(psutil.sensors_temperatures()['coretemp'][0][1]) > 50.0:
                    print('resting to cool down a bit...')
                    time.sleep(60)
                    print('120s ...')
                    time.sleep(20)
                    print('100s ...')
                    time.sleep(20)
                    print('80s ...')
                    time.sleep(20)
                    print('60s ...')
                    time.sleep(20)
                    print('40s ...')
                    time.sleep(20)
                    print('20s ...')
                    time.sleep(20)
                    print('resuming !')

    ############### close tensorboard log ##############
    writer.close()
    ####################################################




# Top level data directory. Here we assume the format of the directory conforms 
#   to the ImageFolder structure
#data_dir = "./data/hymenoptera_data"





# Number of classes in the dataset
#num_classes = 2


# Batch size for training (change depending on how much memory you have)


# Number of epochs to train for 


# Flag for feature extracting. When False, we finetune the whole model, 
#   when True we only update the reshaped layer params
def get_classes_accuracy(model, testloader, classifier_data_dir):

    classes = os.listdir(classifier_data_dir)
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    class_correct = list(0. for i in range(len(classes)))
    class_total = list(0. for i in range(len(classes)))
    print(class_correct)

    
    n = 0
    with torch.no_grad():
        for data in testloader:
            images, labels = data
            images = images.to(device)
            labels = labels.to(device)
            outputs = model(images)
            # print(len(outputs))
            _, predicted = torch.max(outputs, 1)
            # print(predicted)
            # print(labels)
            c = (predicted == labels).squeeze()
            # print(int(c[0].item()))
            # print(len(c))
            label = int(labels[0])
            # # print(label)
            class_correct[label] += int(c[0])
            class_total[label] += 1

    print(class_correct)
    print(class_total)

    sum = 0
    for i in range(len(classes)):
        
        if class_total[i] != 0 :
            print('Accuracy of %5s : %2d %%' % (
                classes[i], 100 * class_correct[i] / class_total[i]))
            
            sum += int(100 * class_correct[i] / class_total[i])
            
    
    av_accuracy = sum / len(classes)
    print("Average accuracy : ",av_accuracy)

    return av_accuracy

"""
Following functions are for the finetuning of classifiers.
cf. https://pytorch.org/tutorials/beginner/finetuning_torchvision_models_tutorial.html
"""


def train_model(model, dataloaders, criterion, optimizer, num_epochs=25, is_inception=False,
checkpoint_save_path='./checkpoints/best_classifier.pth'):
    
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    since = time.time()

    val_acc_history = []
    
    best_model_wts = copy.deepcopy(model.state_dict())
    best_acc = 0.0

    for epoch in range(num_epochs):
        print('Epoch {}/{}'.format(epoch, num_epochs - 1))
        print('-' * 10)

        # Each epoch has a training and validation phase
        for phase in ['train', 'val']:
            if phase == 'train':
                model.train()  # Set model to training mode
            else:
                model.eval()   # Set model to evaluate mode

            running_loss = 0.0
            running_corrects = 0

            # Iterate over data.
            for inputs, labels in dataloaders[phase]:
                inputs = inputs.to(device)
                labels = labels.to(device)

                # zero the parameter gradients
                optimizer.zero_grad()

                # forward
                # track history if only in train
                with torch.set_grad_enabled(phase == 'train'):
                    # Get model outputs and calculate loss
                    # Special case for inception because in training it has an auxiliary output. In train
                    #   mode we calculate the loss by summing the final output and the auxiliary output
                    #   but in testing we only consider the final output.
                    if is_inception and phase == 'train':
                        # From https://discuss.pytorch.org/t/how-to-optimize-inception-model-with-auxiliary-classifiers/7958
                        outputs, aux_outputs = model(inputs)
                        loss1 = criterion(outputs, labels)
                        loss2 = criterion(aux_outputs, labels)
                        loss = loss1 + 0.4*loss2
                    else:
                        outputs = model(inputs)
                        loss = criterion(outputs, labels)

                    _, preds = torch.max(outputs, 1)

                    # backward + optimize only if in training phase
                    if phase == 'train':
                        loss.backward()
                        optimizer.step()

                # statistics
                running_loss += loss.item() * inputs.size(0)
                running_corrects += torch.sum(preds == labels.data)

            epoch_loss = running_loss / len(dataloaders[phase].dataset)
            epoch_acc = running_corrects.double() / len(dataloaders[phase].dataset)

            print('{} Loss: {:.4f} Acc: {:.4f}'.format(phase, epoch_loss, epoch_acc))

            # deep copy the model
            if phase == 'val' and epoch_acc > best_acc:
                best_acc = epoch_acc
                best_model_wts = copy.deepcopy(model.state_dict())
                torch.save(model.state_dict(), checkpoint_save_path)
            if phase == 'val':
                val_acc_history.append(epoch_acc)

        print()

    time_elapsed = time.time() - since
    print('Training complete in {:.0f}m {:.0f}s'.format(time_elapsed // 60, time_elapsed % 60))
    print('Best val Acc: {:4f}'.format(best_acc))

    # load best model weights
    model.load_state_dict(best_model_wts)
    

    return model, val_acc_history

def set_parameter_requires_grad(model, feature_extracting):
    if feature_extracting:
        for param in model.parameters():
            param.requires_grad = False


def initialize_model(model_name, num_classes, feature_extract, use_pretrained=True):

    # Initialize these variables which will be set in this if statement. Each of these
    #   variables is model specific.
    model_ft = None
    input_size = 0

    if model_name == "resnet":
        """ Resnet18
        """
        model_ft = models.resnet18(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        num_ftrs = model_ft.fc.in_features
        model_ft.fc = nn.Linear(num_ftrs, num_classes)
        input_size = 224

    elif model_name == "alexnet":
        """ Alexnet
        """
        model_ft = models.alexnet(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        num_ftrs = model_ft.classifier[6].in_features
        model_ft.classifier[6] = nn.Linear(num_ftrs,num_classes)
        input_size = 224

    elif model_name == "vgg":
        """ VGG11_bn
        """
        model_ft = models.vgg11_bn(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        num_ftrs = model_ft.classifier[6].in_features
        model_ft.classifier[6] = nn.Linear(num_ftrs,num_classes)
        input_size = 224

    elif model_name == "squeezenet":
        """ Squeezenet
        """
        model_ft = models.squeezenet1_0(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        model_ft.classifier[1] = nn.Conv2d(512, num_classes, kernel_size=(1,1), stride=(1,1))
        model_ft.num_classes = num_classes
        input_size = 224

    elif model_name == "densenet":
        """ Densenet
        """
        model_ft = models.densenet121(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        num_ftrs = model_ft.classifier.in_features
        model_ft.classifier = nn.Linear(num_ftrs, num_classes) 
        input_size = 224

    elif model_name == "inception":
        """ Inception v3 
        Be careful, expects (299,299) sized images and has auxiliary output
        """
        model_ft = models.inception_v3(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        # Handle the auxilary net
        num_ftrs = model_ft.AuxLogits.fc.in_features
        model_ft.AuxLogits.fc = nn.Linear(num_ftrs, num_classes)
        # Handle the primary net
        num_ftrs = model_ft.fc.in_features
        model_ft.fc = nn.Linear(num_ftrs,num_classes)
        input_size = 299

    else:
        print("Invalid model name, exiting...")
        exit()
    
    return model_ft, input_size



def train_classifier(data_dir ="./data/train/classifier_data/ant",model_name = "vgg",num_classes = 11,
batch_size = 8,num_epochs = 2,feature_extract = True):

    """
    Models to choose from [resnet, alexnet, vgg, squeezenet, densenet, inception]
    """
    # Initialize the model for this run
    model_ft, input_size = initialize_model(model_name, num_classes, feature_extract, use_pretrained=True)

    # Print the model we just instantiated
    print(model_ft)


    # Data augmentation and normalization for training
    # Just normalization for validation
    data_transforms = {
        'train': transforms.Compose([
            transforms.RandomResizedCrop(input_size),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]),
        'val': transforms.Compose([
            transforms.Resize(input_size),
            transforms.CenterCrop(input_size),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
        ]),
    }

    print("Initializing Datasets and Dataloaders...")

    # Create training and validation datasets
    image_datasets = {x: datasets.ImageFolder(os.path.join(data_dir, x), data_transforms[x]) for x in ['train', 'val']}
    # Create training and validation dataloaders
    dataloaders_dict = {x: torch.utils.data.DataLoader(image_datasets[x], batch_size=batch_size, shuffle=True, num_workers=4) for x in ['train', 'val']}

    # Detect if we have a GPU available
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")



    # Send the model to GPU
    model_ft = model_ft.to(device)

    # Gather the parameters to be optimized/updated in this run. If we are
    #  finetuning we will be updating all parameters. However, if we are 
    #  doing feature extract method, we will only update the parameters
    #  that we have just initialized, i.e. the parameters with requires_grad
    #  is True.
    params_to_update = model_ft.parameters()
    print("Params to learn:")
    if feature_extract:
        params_to_update = []
        for name,param in model_ft.named_parameters():
            if param.requires_grad == True:
                params_to_update.append(param)
                print("\t",name)
    else:
        for name,param in model_ft.named_parameters():
            if param.requires_grad == True:
                print("\t",name)

    # Observe that all parameters are being optimized
    optimizer_ft = optim.SGD(params_to_update, lr=0.001, momentum=0.9)

    # Setup the loss fxn
    criterion = nn.CrossEntropyLoss()

    # Train and evaluate
    model_ft, hist = train_model(model_ft, dataloaders_dict, criterion, optimizer_ft, num_epochs=num_epochs, is_inception=(model_name=="inception"))

    data_val_dir = data_dir + '/val'

    av_accuracy = get_classes_accuracy(model_ft, dataloaders_dict['val'],data_val_dir)


    return model_name, av_accuracy





# train_classifier()
# def train_classifier(classifier_data_dir='data/train/classifier_data/ant'):
#     """
#     Train the classifier
#     """

#     sort_classes_into_directories(classifier_data_dir)

#     transform = transforms.Compose(
#         [transforms.Resize((32,32)),
#             transforms.ToTensor()])

#     train_dataset = datasets.ImageFolder(classifier_data_dir, transform=transform)

#     trainloader = torch.utils.data.DataLoader(train_dataset, batch_size=2, num_workers=8)

#     device = torch.device("cuda" if torch.cuda.is_available() 
#                                     else "cpu")

#     ########################################################################
#     # 2. Define a Convolutional Neural Network
#     # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#     # Copy the neural network from the Neural Networks section before and modify it to
#     # take 3-channel images (instead of 1-channel images as it was defined).

#     num_classes = len(os.listdir(classifier_data_dir))

#     from utils.classifiers import Net

#     # class Net(nn.Module):
#     #     def __init__(self):
#     #         super(Net, self).__init__()
#     #         self.conv1 = nn.Conv2d(3, 6, 5)
#     #         self.pool = nn.MaxPool2d(2, 2)
#     #         self.conv2 = nn.Conv2d(6, 16, 5)
#     #         self.conv3 = nn.Conv2d(6, 16, 5)
#     #         self.conv4 = nn.Conv2d(6, 16, 5)
#     #         self.fc1 = nn.Linear(16 * 5 * 5, 120)
#     #         self.fc2 = nn.Linear(120, 84)
#     #         self.fc3 = nn.Linear(84, num_classes)

#     #     def forward(self, x):
#     #         x = self.pool(F.relu(self.conv1(x)))
#     #         x = self.pool(F.relu(self.conv2(x)))
#     #         x = x.view(-1, 16 * 5 * 5)
#     #         x = F.relu(self.fc1(x))
#     #         x = F.relu(self.fc2(x))
#     #         x = self.fc3(x)
#     #         return x
#     model = models.densenet161(pretrained=True)
#     print(model)


#     # net = Net()
#     # net.fc3 = nn.Linear(84,num_classes)

#     # ########################################################################
#     # # 3. Define a Loss function and optimizer
#     # # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#     # # Let's use a Classification Cross-Entropy loss and SGD with momentum.

#     # criterion = nn.CrossEntropyLoss()
#     # optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

#     # net.to(device)

#     # ########################################################################
#     # # 4. Train the network
#     # # ^^^^^^^^^^^^^^^^^^^^
#     # #
#     # # This is when things start to get interesting.
#     # # We simply have to loop over our data iterator, and feed the inputs to the
#     # # network and optimize.

#     # PATH = './cifar_net.pth'

#     # for epoch in range(10):  # loop over the dataset multiple times

#     #     running_loss = 0.0
#     #     n_batch = 0
#     #     for batch, labels in trainloader:
#     #         batch, labels = batch.to(device), labels.to(device)
#     #         n_batch +=1

#     #         # zero the parameter gradients
#     #         optimizer.zero_grad()

#     #         # forward + backward + optimize
#     #         outputs = net(batch)
#     #         loss = criterion(outputs, labels)
#     #         loss.backward()
#     #         optimizer.step()

#     #         # print statistics
#     #         running_loss += loss.item()
#     #         # if n_batch % 2000 == 1999:    # print every 2000 mini-batches
#     #         if n_batch > 0:
#     #             print('[%d, %5d] loss: %.3f' %
#     #                   (epoch + 1, n_batch + 1, running_loss / 2000))
#     #             running_loss = 0.0
        
        
#     #     torch.save(net.state_dict(), PATH)
#     #     test_classifier()

#     print('Finished Training')

#     ########################################################################
#     # Let's quickly save our trained model:

    

#     ############################################################

#     #
#     #
#     #
#     # TO MOVE TO SEPARATE FUNCTION

#     # test_dataset = datasets.ImageFolder(classifier_data_dir, transform=transform) # to change with train dir 

#     # testloader = torch.utils.data.DataLoader(test_dataset, batch_size=2, num_workers=8)
#     # # print(testloader)

#     # net = Net()
#     # net.fc3 = nn.Linear(84,num_classes)
#     # net.load_state_dict(torch.load(PATH))

#     # correct = 0
#     # total = 0
#     # # n=0
#     # # with torch.no_grad():
#     # #     for data in testloader:
#     # #         # n += 1
#     # #         # print(n)
#     # #         images, labels = data
#     # #         outputs = net(images)
#     # #         _, predicted = torch.max(outputs.data, 1)
#     # #         total += labels.size(0)
#     # #         correct += (predicted == labels).sum().item()

#     # # print('Accuracy of the network on the train images: %d %%' % (100 * correct / total))

#     # classes = os.listdir(classifier_data_dir)

#     # class_correct = list(0. for i in range(len(classes)))
#     # class_total = list(0. for i in range(len(classes)))
#     # print(class_correct)

#     # with torch.no_grad():
#     #     for data in testloader:
#     #         # n += 1
#     #         # print(n)
#     #         images, labels = data
#     #         label = int(labels[0])
#     #         outputs = net(images)
#     #         _, predicted = torch.max(outputs.data, 1)
#     #         total += labels.size(0)
#     #         correct += (predicted == labels).sum().item()
#     #         class_correct[label] += (predicted == labels).sum().item()
#     #         class_total[label] += labels.size(0)

#     #         # for i in range(len(classes)):


#     # # n = 0
#     # # with torch.no_grad():
#     # #     for data in testloader:
#     # #         n += 1
#     # #         print(n)
#     # #         images, labels = data
#     # #         outputs = net(images)
#     # #         # print(outputs)
#     # #         _, predicted = torch.max(outputs, 1)
#     # #         # print(predicted)
#     # #         # print(labels)
#     # #         c = (predicted == labels).squeeze()
#     # #         # print(int(c[0].item()))
#     # #         print(len(c))
#     # #         # # label = int(labels[0])
#     # #         # # print(label)
#     # #         # # class_correct[label] += int(c[0])
#     # #         # # class_total[label] += 1

#     # print(class_correct)
#     # print(class_total)


#     # for i in range(len(classes)):
#     #     if class_total[i] != 0 :
#     #         print('Accuracy of %5s : %2d %%' % (
#     #             classes[i], 100 * class_correct[i] / class_total[i]))



#     return



