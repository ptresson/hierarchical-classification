from __future__ import print_function
import os
import cv2
import pandas as pd
import numpy as np
import time
from PIL import Image
import subprocess

from utils.utils import load_bbox, list_files, sort_classes_into_directories, clean_dir, list_files, get_last_file
from utils.extract_objects import crop_im_test_imagewise, crop_im_test_imagewise_DEBUG
from utils.plot_bbox import plot_bbx
from utils.Calc_IoU import calc_IOU
from utils.preprocessing import slice_train_valid, slice_test, sort_empty_slices, update_data_file
from utils.postprocessing import multi_proc_manager
from utils.extract_objects import crop_multi, crop_im_train

#from utils.performances_analysis import get_confusion_matrix
# from utils.classification import Classification # still bugs to fix in classification


pd.set_option('display.expand_frame_repr', False)

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.transforms as transforms
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision import datasets
import torch.optim as optim
import torchvision.models as models
# from torch.utils.tensorboard import SummaryWriter

def class_threshold(detection_csv='output/post_classif.csv'):

    df = pd.read_csv(detection_csv)

    d = {
        'brachymyrmex_aphidicola':0.2,
        'cheilomenes_sulphurea':0.2,
        'cyphomyrmex_rimosus':0.7,
        'dysmicoccus_brevipes':0.98,
        'exochomus_laeviusculus':0.98,
        'icerya_seychellarum':0.2,
        'isometrus_maculatus':0.95,
        'lycosidae_msp1':0.2,
        'lycosidae_msp2':0.8,
        'pachybolidae_msp1':0.45,
        'paradoxosomatidae_msp1':0.45,
        'paratrechina_longicornis':0.95,
        'pheidole_megacephala_major': 0.95,
        'pheidole_megacephala_minor': 0.4,
        'psyllobora_variegata':0.2,
        'salticidae_msp1':0.2,
        'salticidae_msp2':0.95,
        'solenopsis_geminata_minor': 0.69,
        'tapinoma_melanocephalum':0.9,
        'technomyrmex_albipes':0.95,
        'tetramorium_bicarinatum':0.8,
        'ceroplastes_sinensis':0.2
     }

    
    for row in df.iterrows():
        index, data = row
        for obj_class, threshold in d.items():
            if (str(data.obj_class) == str(obj_class) and float(data.classif_confidence)<float(threshold)):
                df.at[index, 'obj_class'] = 'to_check'

    
    df.to_csv('output/post_classif_threshold.csv')

    return





def get_detection_metrics(test_im_dir,detection_csv_file, names_file, print_res=True, edit_res=False):


    ################### function to get average precision ################################

    def voc_ap(rec, prec):
        # from https://github.com/Cartucho/mAP/blob/master/main.py
        """
        --- Official matlab code VOC2012---
        mrec=[0 ; rec ; 1];
        mpre=[0 ; prec ; 0];
        for i=numel(mpre)-1:-1:1
            mpre(i)=max(mpre(i),mpre(i+1));
        end
        i=find(mrec(2:end)~=mrec(1:end-1))+1;
        ap=sum((mrec(i)-mrec(i-1)).*mpre(i));
        """
        rec.insert(0, 0.0)  # insert 0.0 at begining of list
        rec.append(1.0)  # insert 1.0 at end of list
        mrec = rec[:]
        prec.insert(0, 0.0)  # insert 0.0 at begining of list
        prec.append(0.0)  # insert 0.0 at end of list
        mpre = prec[:]
        """
         This part makes the precision monotonically decreasing
          (goes from the end to the beginning)
          matlab:  for i=numel(mpre)-1:-1:1
                      mpre(i)=max(mpre(i),mpre(i+1));
        """
        # matlab indexes start in 1 but python in 0, so I have to do:
        #   range(start=(len(mpre) - 2), end=0, step=-1)
        # also the python function range excludes the end, resulting in:
        #   range(start=(len(mpre) - 2), end=-1, step=-1)
        for i in range(len(mpre) - 2, -1, -1):
            mpre[i] = max(mpre[i], mpre[i + 1])
        """
         This part creates a list of indexes where the recall changes
          matlab:  i=find(mrec(2:end)~=mrec(1:end-1))+1;
        """
        i_list = []
        for i in range(1, len(mrec)):
            if mrec[i] != mrec[i - 1]:
                i_list.append(i)  # if it was matlab would be i + 1
        """
         The Average Precision (AP) is the area under the curve
          (numerical integration)
          matlab: ap=sum((mrec(i)-mrec(i-1)).*mpre(i));
        """
        ap = 0.0
        for i in i_list:
            ap += ((mrec[i] - mrec[i - 1]) * mpre[i])
        return ap, mrec, mpre


    ###########################################################################################################

    ######################## Formating both detection and GT data into same template ##########################

    file_bbox = sorted([f for f in os.listdir(test_im_dir) if f.endswith('.txt')])
    images = sorted([f for f in os.listdir(test_im_dir) if f.endswith('.JPG') or f.endswith('.jpg')])

    # sort only images with labels and labels attached to images

    image_labelled = []

    for i in range(len(file_bbox)):
        root_name_bbx = file_bbox[i].split('.')[0]
        for image in images:
            if image.split('.')[0] == root_name_bbx:
                image_labelled.append(image)

    image_labelled = sorted(image_labelled)

    labels_with_images = []

    for i in range(len(image_labelled)):
        root_name_image = image_labelled[i].split('.')[0]
        for label in file_bbox:
            if label.split('.')[0] == root_name_image:
                labels_with_images.append(label)

    labels_with_images = sorted(labels_with_images)

    bbox = load_bbox(labels_with_images,test_im_dir)

    merged_bbox_list = []

    for i in range(len(bbox)):
        for j in range(len(bbox[i])):
            bbox[i][j].insert(0, images[i][0:-4])
        merged_bbox_list = merged_bbox_list + bbox[i]

    ground_truth = merged_bbox_list

    detection_csv = detection_csv_file


    # gt_df stores GT information
    # dt_df stores detection information
    gt_df = pd.DataFrame(ground_truth, columns=('root_image','obj_class', 'x', 'y', 'w', 'h'))

    print('GT classes :')
    print(gt_df.obj_class.unique())

    dt_df = pd.read_csv(detection_csv)
    dt_df = dt_df[dt_df['obj_class'] != 'to_check']

    ######################### convert classes according .names file ###################

    gt_df['obj_class'] = gt_df.obj_class.astype(str)
    
    with open(names_file) as f:
        labels = [line.rstrip('\n') for line in f]


    for row in gt_df.iterrows():
        index, data = row
        for i in range(len(labels)):
            if (row[1].obj_class == str(float(i))):
                gt_df.at[index, 'obj_class'] = labels[i]

    ################## compare only images that have associated GT ####################

    dt_df = dt_df[dt_df.root_image.isin(gt_df.root_image.unique())]

    ################## get the list of all classes (detected of not) ####################

    class_list = []

    for dift_class in gt_df.obj_class.unique():
        class_list.append(dift_class)



    for dift_class in dt_df.obj_class.unique():
        if dift_class not in class_list:
            class_list.append(dift_class)


    ########################### generating template dict for per classes metrics ###############

    mAP_class_dict = {}
    for dift_class in class_list :
        mAP_class_dict[dift_class] = 0

    ####################### initialize variables #############################

    dt_df = dt_df.sort_values(['obj_class'])
    dt_df['correct'] = 0
    dt_df['precision'] = 0.0
    dt_df['recall'] = 0.0

    gt_df = gt_df.sort_values(['obj_class'])
    gt_df['used'] = 0

    # overlap threshold for acceptance
    overlap_threshold = 0.5


    ####################### comparison of detected objects  to GT #################################

    for image in dt_df.root_image.unique():

        # print(image)

        dt_df_img = dt_df[dt_df['root_image'] == image]
        gt_df_img = gt_df[gt_df['root_image'] == image]

        # list different classes present in GT

        gt_classes = gt_df_img.obj_class.unique()

        # list out wrong predicted classes

        dt_df_correct_classes = dt_df_img[dt_df_img['obj_class'].isin(gt_classes)]

       # for each  correct class

        for dift_class in gt_classes:

           gt_df_class = gt_df_img[gt_df_img.obj_class == dift_class]

           dt_df_class = dt_df_correct_classes[dt_df_correct_classes.obj_class == dift_class]

           for rowA in gt_df_class.iterrows():  # compare each GT object of this class ...

                xA = rowA[1].x
                yA = rowA[1].y
                wA = rowA[1].w
                hA = rowA[1].h
                
                for rowB in dt_df_class.iterrows():  # ... with every detected object of this class

                    xB = rowB[1].x
                    yB = rowB[1].y
                    wB = rowB[1].w
                    hB = rowB[1].h

                    IOU = calc_IOU(xA, xB, yA, yB, wA, wB, hA, hB)

                    if IOU > overlap_threshold :  # i.e. if detection and GT overlap

                        if rowA[1].used == 0:  # gt not found yet
                            indexB, dataB = rowB
                            dt_df.at[indexB, 'correct'] = 1

                            indexA, dataA = rowA
                            gt_df.at[indexA, 'used'] = 1
                            rowA[1].used = 1

    df_TP = dt_df[dt_df.correct == 1]
    df_FP = dt_df[dt_df.correct == 0]
    df_FN = gt_df[gt_df.used == 0]
    # print(df_FP)
    # print(df_FN)
    df_TP.to_csv('output/TP.csv', index=False)
    df_FP.to_csv('output/FP.csv', index=False)
    df_FN.to_csv('output/FN.csv', index=False)

    TP = len(df_TP.correct)
    FP = len(df_FP.correct)
    FN = len(df_FN.used)


    precision_general = float(TP / (TP + FP))
    recall_general = float(TP / (TP + FN))
    
    if precision_general == recall_general == 0:
    
        print('\nprecision and recall to zero. Maybe are the wrong classes used ?\n')
        F1_general = 0
    
    else:
        F1_general = 2 * float((precision_general * recall_general) / (precision_general + recall_general))


    ########################### mAP #############################

    # once we have for each detection its status (TP or FP), we can compute average precision for each class
    # for this we will need to compute recall and precision for each class

    # for dift_class in gt_df.obj_class.unique():
    for dift_class in dt_df.obj_class.unique():

        gt_df_class = gt_df[gt_df.obj_class == dift_class]
        dt_df_class = dt_df[dt_df.obj_class == dift_class]

        # GT number of objects to find (potential positives = PP)

        PP = len(gt_df_class)

        if PP == 0: # if false positive
            mAP_class_dict[dift_class] = 0

        if PP != 0:
            TP_count = 0
            row_count = 0  # (for recall)

            for row in dt_df_class.iterrows():

                row_count += 1
                index, data = row

                if (data['correct'] == 1):
                    TP_count += 1

                recall = float(TP_count / PP)
                precision = float(TP_count / row_count)

                dt_df_class.at[index, 'recall'] = recall
                dt_df_class.at[index, 'precision'] = precision

            prec = dt_df_class['precision'].tolist()
            rec = dt_df_class['recall'].tolist()
            ap, mrec, mprec = voc_ap(rec, prec)  # according previously defined function

            mAP_class_dict[dift_class] += ap

    # this is just a fix for some issues pandas can encounter with .from_dict() function
    # but sometimes, it does work perfectly fine.
    new_dict = {k: [v] for k, v in mAP_class_dict.items()}
    temp_df = pd.DataFrame(new_dict, index = ['mAP'])
    mAP_class_df = temp_df.T
    # mAP_class_df = pd.DataFrame.from_dict(mAP_class_dict, orient = 'index', columns = ['mAP'])


    mAP = float(float(sum(mAP_class_dict.values()))/float(len(class_list)))

    if print_res:
        print('\nper class_______________________\n')
        print(mAP_class_df)
        print('\ngeneral_________________\n')
        print('mAP: \t\t%.4f' % mAP)
        print('precision: \t%.4f' % precision_general)
        print('recall: \t%.4f' % recall_general)
        print('F1: \t\t%.4f' % F1_general)

        #################### edit results to csv file

    # if edit_res:
    #     res_list = [['mAP', mAP], ['precision', precision], ['recall', recall], ['F1', F1_general]]
    #     res_df = pd.DataFrame(res_list, colums=['metric', 'value'])
    #     res_df.to_csv(outname, index=False)

    return TP, FP, FN, precision_general, recall_general, F1_general, mAP, mAP_class_dict





def image_loader(loader, image_name):
    image = Image.fromarray(image_name.astype('uint8'), 'RGB')
    image = loader(image).float()
    image = torch.tensor(image, requires_grad=True)
    return image

## Old transforms Cf transfo in predict_image().

# test_transforms = transforms.Compose([transforms.Resize((32,32)),
#                                       transforms.ToTensor(),
#                                      ])

# data_transforms = transforms.Compose([
#     transforms.Resize(256),
#     transforms.CenterCrop(224),
#     transforms.ToTensor()
# ])

# data_transforms = transforms.Compose([
#             transforms.Resize(input_size),
#             transforms.CenterCrop(input_size),
#             transforms.ToTensor(),
#             transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
# ])



# may need to change transforms for the good model
def predict_image(model,image,input_size):

    data_transforms = transforms.Compose([
            transforms.Resize(input_size),
            transforms.CenterCrop(input_size),
            transforms.ToTensor(),
            transforms.Normalize([0.485, 0.456, 0.406], [0.229, 0.224, 0.225])
    ])

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    image_tensor = data_transforms(image).float()
    image_tensor = image_tensor.unsqueeze_(0)
    input = Variable(image_tensor)
    input = input.to(device)
    output = model(input)
    output_softmax = torch.nn.functional.softmax(output[0], dim=0)
    # print(output_softmax.data.cpu().numpy())
    # print(output_softmax.data.cpu().numpy().argmax())
    index = output_softmax.data.cpu().numpy().argmax()
    conf = output_softmax.data.cpu().numpy()[index]
    return index, conf


def set_parameter_requires_grad(model, feature_extracting):
    if feature_extracting:
        for param in model.parameters():
            param.requires_grad = False

def initialize_model(model_name, num_classes, feature_extract, use_pretrained=True):

    # Initialize these variables which will be set in this if statement. Each of these
    #   variables is model specific.
    model_ft = None
    input_size = 0

    if model_name == "resnet":
        """ Resnet18
        """
        model_ft = models.resnet18(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        num_ftrs = model_ft.fc.in_features
        model_ft.fc = nn.Linear(num_ftrs, num_classes)
        input_size = 224

    elif model_name == "alexnet":
        """ Alexnet
        """
        model_ft = models.alexnet(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        num_ftrs = model_ft.classifier[6].in_features
        model_ft.classifier[6] = nn.Linear(num_ftrs,num_classes)
        input_size = 224

    elif model_name == "vgg":
        """ VGG11_bn
        """
        model_ft = models.vgg11_bn(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        num_ftrs = model_ft.classifier[6].in_features
        model_ft.classifier[6] = nn.Linear(num_ftrs,num_classes)
        input_size = 224

    elif model_name == "squeezenet":
        """ Squeezenet
        """
        model_ft = models.squeezenet1_0(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        model_ft.classifier[1] = nn.Conv2d(512, num_classes, kernel_size=(1,1), stride=(1,1))
        model_ft.num_classes = num_classes
        input_size = 224

    elif model_name == "densenet":
        """ Densenet
        """
        model_ft = models.densenet121(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        num_ftrs = model_ft.classifier.in_features
        model_ft.classifier = nn.Linear(num_ftrs, num_classes) 
        input_size = 224

    elif model_name == "inception":
        """ Inception v3 
        Be careful, expects (299,299) sized images and has auxiliary output
        """
        model_ft = models.inception_v3(pretrained=use_pretrained)
        set_parameter_requires_grad(model_ft, feature_extract)
        # Handle the auxilary net
        num_ftrs = model_ft.AuxLogits.fc.in_features
        model_ft.AuxLogits.fc = nn.Linear(num_ftrs, num_classes)
        # Handle the primary net
        num_ftrs = model_ft.fc.in_features
        model_ft.fc = nn.Linear(num_ftrs,num_classes)
        input_size = 299

    else:
        print("Invalid model name, exiting...")
        exit()
    
    return model_ft, input_size


def Classification(cfg_dir = 'config',classifier_data_dir='data/train/classifier_data/ant/train',
 names_convert_file = 'config/insects_convert.names',test_im_dir='data/test/original_detailled',
 refined_detection_file='output/refined_detections_general.csv', confidence_threshold = 0.2, 
 trained_model_path = 'checkpoints/best_classifier.pth'):

    t0 = time.time()
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    
    # create new refined detection
    refined_df_original = pd.read_csv(refined_detection_file)
    refined_df = refined_df_original.copy(deep=True)
    
    # add column obj_class_detailled
    refined_df['sub_class'] = 'pachybolidae_msp1'
    refined_df['check_status'] = 'valid'
    refined_df['index'] = 'NA'
    refined_df['gt_class'] = 'NA'

    # add column classification_confidence
    refined_df['classif_confidence'] = 0.0

    test_image_list = [f for f in os.listdir(test_im_dir) if f.endswith('.JPG') or f.endswith('.jpg')]

    sort_classes_into_directories(classifier_data_dir)
    classes = sorted(os.listdir(classifier_data_dir))
    num_classes = len(classes)
    print(classes)
    print(num_classes)
    # model_name='densenet'
    model_name = 'alexnet'
    #########################################################""

    # initialize model
    model_ft, input_size = initialize_model(model_name, num_classes, feature_extract=True, use_pretrained=True)
    
    # load model
    model_ft.load_state_dict(torch.load(trained_model_path))
    print(model_ft)
    model_ft.to(device)
    model_ft.eval()
    n=len(refined_df['sub_class'])
    i=0

    # iterate over all detected objects
    for row in refined_df.iterrows():

        i+=1

        index, data = row

        if data.obj_class == 'ant':

            # extract
            image = crop_im_test_imagewise(row, test_image_list,test_im_dir)
            image = Image.fromarray(image)

            # classify
            class_int, conf = predict_image(model_ft,image, input_size)

            # print(class_int)

            subclass = classes[class_int]
            print("(%d/%d) %s %2f" %(i,n,subclass, conf))
            
            # replace subclass
            refined_df.at[index, 'sub_class'] = subclass
            refined_df.at[index, 'classif_confidence'] = conf

    refined_df.rename(columns={'obj_class': 'super_class', 'sub_class': 'obj_class'}, inplace=True)
    refined_df.to_csv('output/post_classif.csv', index=False)  
    
    
    t1 = time.time()

    duration = t1-t0

    print('\nClassification time (s):')
    print(duration)
    
    return

## same as above but loops through all classes

def Classification_all(cfg_dir = 'config',classifier_data_dir='data/train/classifier_data/',
 names_convert_file = 'config/insects_convert.names',test_im_dir='data/test/original_detailled',
 refined_detection_file='output/refined_detections_gal_newer.csv', confidence_threshold = 0.2):

    t0 = time.time()
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    
    # create new refined detection
    refined_df_original = pd.read_csv(refined_detection_file)
    refined_df = refined_df_original.copy(deep=True)
    
    # add column obj_class_detailled
    refined_df['sub_class'] = 'NA'
    refined_df['check_status'] = 'valid'
    refined_df['index'] = 'NA'
    refined_df['gt_class'] = 'NA'

    # add column classification_confidence
    refined_df['classif_confidence'] = 0.0

    test_image_list = [f for f in os.listdir(test_im_dir) if f.endswith('.JPG') or f.endswith('.jpg')]

    # chosen model after benchmarking
    model_name = 'squeezenet'

    #########################################################
    
    # for all super classes
    for super_class in os.listdir(classifier_data_dir):

        super_class_dir = classifier_data_dir + super_class + '/val'
        sort_classes_into_directories(super_class_dir)
        classes = sorted(os.listdir(super_class_dir))
        num_classes = len(classes)
        print(super_class)
        print(classes)
        print(num_classes)

        # get corresponding trained classifier
        trained_model_path = 'checkpoints/' + super_class + '.pth'
    
        # initialize model
        model_ft, input_size = initialize_model(model_name, num_classes, feature_extract=True, use_pretrained=True)
        
        # load model
        model_ft.load_state_dict(torch.load(trained_model_path))
        print(model_ft)
        model_ft.to(device)
        model_ft.eval()
        n=len(refined_df['sub_class'])
        i=0

        # iterate over all detected objects
        for row in refined_df.iterrows():

            i+=1

            index, data = row

            if data.obj_class == super_class:

                # extract
                image = crop_im_test_imagewise(row, test_image_list,test_im_dir)
                
                # VERY IMPORTANT LINE : crop_im_test uses OpenCV, which does not have the same standard as PIL BGR and not RGB 
                image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                
                image = Image.fromarray(image)
                # image.show()                        

                # classify
                class_int, conf = predict_image(model_ft,image, input_size)

                # print(class_int)

                subclass = classes[class_int]
                print("(%d/%d) %s %2f" %(i,n,subclass, conf))
                
                # replace subclass
                refined_df.at[index, 'sub_class'] = subclass
                refined_df.at[index, 'classif_confidence'] = conf
        
        print("resting a bit....")
        time.sleep(60)

    refined_df.rename(columns={'obj_class': 'super_class', 'sub_class': 'obj_class'}, inplace=True)
    #refined_df.to_csv('output/post_classif.csv', index=False)  
    
    
    t1 = time.time()

    duration = t1-t0

    print('\nClassification time (s):')
    print(duration)
    
    return




Classification_all()
#get_detection_metrics('data/test/original_detailled','output/post_classif.csv','data/classes.names',edit_res=True)


# get_confusion_matrix('data/test/original_detailled',
# 'output/post_classif.csv','data/classes.names','output/confusion_matrix_CH.csv')
