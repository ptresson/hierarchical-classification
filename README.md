# Hierarchical classification

This work was published in  [IEEE Access](https://ieeexplore.ieee.org/document/9411844). The paper is free of access and describes more precisely the functionning of the pipeline. This work is derived from the model proposed in [this paper](https://besjournals.onlinelibrary.wiley.com/doi/abs/10.1111/2041-210X.13281) for the detection of small objects on large input images and the analysis of the interactions between these objects. The version proposed here includes hierarchical classification to imporve the robstness of the detections. The dataset we worked on can be found [here](https://www.lirmm.fr/~wpuech/dataset/HierarchicalClassification/). 
A push mirror of this repo is available [on GitHub](https://github.com/ptresson/Hierarchical-Classification).

![Summary figure](https://gitlab.com/ptresson/hierarchical-classification/-/blob/master/recap.png)

The main idea is to train a general detector on species labelled with general super-classes (e.g. all ants into the `ant` super-class, all spiders into the `spider` super-class) and then to classify the detected object with specific classifiers (one for ants, one for spiders etc.). For now, the training, testing and use of the different models needed (general detector, different classifiers) is done separately. A 'flat' detector trained directly at species level on the same dataset was used to compare the results.

The scripts in this repository integrate a slicing step to handle large input files for the detection of small objects, the training and testing of the detector (we use a [PyTorch implementation of YOLOv3](https://github.com/eriklindernoren/PyTorch-YOLOv3)), and training and testing of classifiers for the hierarchical step. Pre- and post-processing of images and detections, traing and testing of the models is regrouped in `main.py`. After the processing steps surrounding the detection of objects, detections are stored in a csv file `output/refined_detections.csv`. The hierarchical classification step is handled separatly in the `classification_standalone.py` script once the models are trained and general detections stored in `output/refined_detections_gal.csv`.

## Requierements

Neural network related computations require CUDNN >= 7.0, CUDA >= 9.0 and OpenCV >= 2.4 (optional). Interactions require Python 3.6, numpy and pandas. Analysis scripts work best with python-opencv but are possible with PIL. Matplotlib is used for ploting the results.

## Running with docker.

We provide a Dockerfile to generate a functionning docker image. Once built, you can work within containers and not have to worry about configurations, requierements, or eventual problems damaging your system. This docker image is based on a ubuntu 16.04 image with CUDA 9.0, CUDNN 7.6. python 3 and OpenCV 3.2.

To create a docker, you first have to install [nvidia-docker](https://github.com/NVIDIA/nvidia-docker).

Then, buid the docker image from the `/HierarchicalClassification` directory using `nvidia-docker build --no-cache -t hierarchicalclassif .`, where `hierarchicalclassif`can be any name you want.

Once the image built, to run a container using this image, use `nvidia-docker run -it -v /path/to/your/working/directory:/path/to/your/working/directory --name your_container_name hierarchicalclassif`.

You can then work inside your cointainer.

To quit the container, use `ctrl+p` `ctrl+q`. To stop the container, use `docker stop your_container_name`. To remove the container, use `docker rm your_container_name`.

## Workflow

For now, the best way to use these models is to :

- Convert the labels of the dataset at super-class level (while keeping the detailled labels as well for testing at the end)
- Train the general detector
- Test/use the detector to generate object detections at super-class level (for instance here in `output/refined_detections_gal.csv`)
- Extract objects from the train dataset and train a classifier for each super-class
- Run the hierarchical classification step to classify the detected objects.
- Test the performances at the end of the process

## Classes mode

Conversion of classes is currently handled in the `conversion_standalone.py` script. Classes labels have to be switched between the detailled labels (at species level) and general labels (at super-class level) before traing the general detector. Images labelled at super-class level are intend to be kept in the `data/train/original` and `data/test/original` folders. Images labelled at super-class level are intend to be kept in the `data/train/original_detailled` and `data/test/original_detailled` folders. 

## Plot the result

Plotting the results is currently handled in the `plot_bbox_analysis_standalone.py` script.

## Performance analysis

Performance analysis is currently handled in the `performance_analysis_standalone.py` script.

## Not using OpenCV

We use OpenCV since it is faster than PIL to precess images. However, all OpenCV version are not compatible with our setup (prefer version <4.0) and OpenCV can be complicated to install depending on your setup. For these reasons, we also provide a version of the slicing using PIL. We provide alterate functions for slicing operations using PIL rather than OpenCV.


