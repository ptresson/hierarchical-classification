import os
from os import listdir
from os.path import isfile, join
import shutil

current_dir = os.path.dirname(os.path.abspath(__file__))


extension_label = '.txt'
extension_image = '.png'
label_dir = os.path.join(current_dir,'labels')
images_dir = os.path.join(current_dir,'images')
background_only_images = os.path.join(images_dir,'without_labels')

# Prendre les fichiers du dossiers en .JPG

images_files = [f for f in os.listdir(images_dir) if f.endswith('.png')]
labels_files = [f for f in os.listdir(label_dir) if f.endswith('.txt')]

images_without_ext = []
for image_name in images_files:
    images_without_ext.append(image_name.split('.')[0])

labels_without_ext = []
for label_name in labels_files:
    labels_without_ext.append(label_name.split('.')[0])

images_with_labels = set(images_without_ext).intersection(labels_without_ext)

images_with_labels_list = list(images_with_labels)
print(images_with_labels_list)

images_with_labels_ext = [name + '.png' for name in images_with_labels_list]


print(images_with_labels_ext)

for image in images_files:
    if image not in images_with_labels_ext:
        source = os.path.join(images_dir, image)
        dest = os.path.join(background_only_images, image)
        shutil.move(source, dest)