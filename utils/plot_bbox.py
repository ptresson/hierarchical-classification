import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib import cm
from PIL import Image
import numpy as np
import pandas as pd
import os
from utils.utils import load_bbox_convert, load_bbox


def get_gt_df(test_im_dir, names_file='data/classes.names'):

    file_bbox = sorted([f for f in os.listdir(test_im_dir) if f.endswith('.txt')])
    images = sorted([f for f in os.listdir(test_im_dir) if f.endswith('.JPG') or f.endswith('.jpg')])

    # sort only images with labels and labels attached to images

    image_labelled = []

    for i in range(len(file_bbox)):
        root_name_bbx = file_bbox[i].split('.')[0]
        for image in images:
            if image.split('.')[0] == root_name_bbx:
                image_labelled.append(image)

    image_labelled = sorted(image_labelled)

    labels_with_images = []

    for i in range(len(image_labelled)):
        root_name_image = image_labelled[i].split('.')[0]
        for label in file_bbox:
            if label.split('.')[0] == root_name_image:
                labels_with_images.append(label)

    labels_with_images = sorted(labels_with_images)

    bbox = load_bbox(labels_with_images,'data/test/original')
    # bbox = load_bbox_convert(labels_with_images, test_im_dir, 'cfg/insects_convert.names','cfg/insects_simple.names')
    
    # print(bbox)


    # for image in bbox:
    #     for obj in image:
    #         if obj[0]>5:
    #             print(image)


    merged_bbox_list = []

    for i in range(len(bbox)):
        for j in range(len(bbox[i])):
            bbox[i][j].insert(0, images[i][0:-4])
        merged_bbox_list = merged_bbox_list + bbox[i]

    ground_truth = merged_bbox_list


    # gt_df stores GT information
    # dt_df stores detection information
    gt_df = pd.DataFrame(ground_truth, columns=('root_image','obj_class', 'x', 'y', 'w', 'h'))

    print('GT classes :')
    print(gt_df.obj_class.unique())


    ######################### convert classes according .names file ###################

    gt_df['obj_class'] = gt_df.obj_class.astype(str)
    
    with open(names_file) as f:
        labels = [line.rstrip('\n') for line in f]


    for row in gt_df.iterrows():
        index, data = row
        for i in range(len(labels)):
            if (row[1].obj_class == str(float(i))):
                gt_df.at[index, 'obj_class'] = labels[i]
    
    return gt_df

def plot_bbx(source_img, coordinates_csv, test_im_dir='test_images', show=True,
 save = False, outname='plot.jpg', res=400, plot_GT=True, plot_DT=True) :

    #df = pd.DataFrame(coordinates_list, columns=('root_image', 'obj_class', 'confidence', 'x', 'y', 'w', 'h'))

    gt_df = get_gt_df(test_im_dir)

    df = pd.read_csv(coordinates_csv)
    root_name_path = source_img.split(".")[0]
    root_name = root_name_path.split('/')[-1]
    df = df[df.root_image == root_name]
    gt_df = gt_df[gt_df.root_image == root_name]

    im = np.array(Image.open(source_img), dtype=np.uint8)
    nH = im.shape[0]
    nW = im.shape[1]


    print('\n\n=============================\n')
    dt_classes = list(df.obj_class.unique())
    print("DT classes :")
    print(dt_classes)
    gt_classes = list(gt_df.obj_class.unique())
    print("GT classes :")
    print(gt_classes)
    classes = dt_classes + gt_classes
    classes = list(set(classes))
    print(classes)

    colors = cm.rainbow(np.linspace(0, 1, len(classes)))

    # Create figure and axes
    fig, ax = plt.subplots(1)

    # Display the image
    ax.imshow(im)

    if plot_DT :

        for row in df.iterrows():

            # left_x = row[1].x
            # top_y = row[1].y
            # w_obj = row[1].w
            # h_obj = row[1].h

            left_x = int((row[1].x - 0.5 * row[1].w) * nW)
            top_y = int((row[1].y - 0.5 * row[1].h) * nH)

            w_obj = row[1].w * nW
            h_obj = row[1].h * nH

            # Create a Rectangle patch

            # different color for each class

            for i in range(len(classes)):

                if row[1].obj_class == classes[i]:
                    color_obj = colors[i]

            rect = patches.Rectangle((left_x, top_y), w_obj, h_obj, linewidth=1, edgecolor=color_obj, facecolor='none')

            # Add the patch to the Axes
            ax.add_patch(rect)
    
    if plot_GT:
        
        for row in gt_df.iterrows():
        # todo :  source for x,y, w, h GT

            left_x_GT = int((row[1].x - 0.5 * row[1].w) * nW)
            top_y_GT = int((row[1].y - 0.5 * row[1].h) * nH)

            w_obj_GT = row[1].w * nW
            h_obj_GT = row[1].h * nH

            # Create a Rectangle patch

            # different color for each class

            for i in range(len(classes)):

                if row[1].obj_class == classes[i]:
                    color_obj = colors[i]

            rect = patches.Rectangle((left_x_GT, top_y_GT), w_obj_GT, h_obj_GT, linewidth=1, edgecolor=color_obj, facecolor='none',linestyle='--')

            # Add the patch to the Axes
            ax.add_patch(rect)

    for i in range(len(classes)):

        ax.text(0.99, 1.01+float(i/45), classes[i],
                verticalalignment='bottom', horizontalalignment='right',
                transform=ax.transAxes,
                color=colors[i], fontsize=5)

    if show:
        plt.show()

    if save:
        plt.savefig(outname, dpi=res)

