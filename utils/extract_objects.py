from __future__ import print_function
import os
import time
import cv2
import pandas as pd
import numpy as np
from utils.utils import load_bbox
import multiprocessing

def chunkIt(seq, num):
    avg = len(seq) / float(num)
    out = []
    last = 0.0

    while last < len(seq):
        out.append(seq[int(last):int(last + avg)])
        last += avg

    return out

def crop_im_test(detection_df, test_images_list, orig_dir = 'test_images',
 outdir = 'test_temp/objects', outname='refined_detection_class', 
 add_bg=False, classifier_input_width = 416):

    dt_df = detection_df

    for row in dt_df.iterrows():

        index, data = row

        image = data.root_image

        image_path = image + '.JPG'

        if image_path in test_images_list:

            image_path_tot = os.path.join(orig_dir, image_path)

            image0 = cv2.imread(image_path_tot, 1)  # color

            win_h, win_w = image0.shape[:2]
            nH = image0.shape[0]
            nW = image0.shape[1]

            x = int(row[1].x * nW)
            y = int(row[1].y * nH)
            w = int(row[1].w * nW)
            h = int(row[1].h * nH)

            half_w = int(w / 2)
            half_h = int(h / 2)
            # print(half_h,half_w)
            # print(y - half_h,y + half_h,x - half_w,x + half_w)

            if(y - half_h < 0):
                half_h = y
            if(y + half_h > nH):
                half_h = nH - y
            if(x - half_w < 0):
                half_w = x
            if(x + half_w > nW):
                half_w = nW - x
            

            # get cropped image at location
            window_c = image0[(y - half_h):(y + half_h), (x - half_w):(x + half_w)]

            outname = str(index)+'-'+str(data.obj_class)+'.png'

            outpath = os.path.join(outdir, outname)

            # print(window_c.shape[1])
            # print(window_c.shape[0])
            
            if add_bg:
                
                # add blank bg
                #ensures criooed image isnt bigger than canevas
                

                if h > classifier_input_width :
                    window_c = cv2.resize(window_c, (w, classifier_input_width))
                    half_h = int(classifier_input_width/2)
                    h = classifier_input_width
                    print('largeur')
                    print(window_c.shape[1])
                    print(window_c.shape[0])


                if w > classifier_input_width :
                    window_c = cv2.resize(window_c, (classifier_input_width,h))
                    half_w = int(classifier_input_width / 2)
                    print('on est là')
                    # cv2.imshow('image', window_c)
                    # cv2.waitKey(0)
                    # cv2.destroyAllWindows()


                # create canevas
                bg_img = np.zeros((classifier_input_width,classifier_input_width,3), np.uint8)
                # bg_img[:,0:classifier_input_width] = (228,201,154)
                bg_img[:, 0:classifier_input_width] = (154, 201, 228)
                x_offset=int((classifier_input_width/2) - half_w)
                y_offset = int((classifier_input_width/2) - half_h)
                bg_img[y_offset:y_offset+window_c.shape[0], x_offset:x_offset+window_c.shape[1]] = window_c
                cv2.imwrite(outpath, bg_img)
            
            else:

                cv2.imwrite(outpath, window_c)
            

        # else:
        #     print("Image %s not found" % image_path)
    return


def crop_im_train(orig_dir = 'data/train/original_detailled', outdir = 'data/train/classifier_data',mode='train',
 names_convert = 'config/insects_convert.names', super_class = 'ant',classifier_input_width = 416, 
 class_balance = 1000, add_size = False):

    # read detection csv
    file_bbox = sorted([f for f in os.listdir(orig_dir) if f.endswith('.txt')])
    images = sorted([f for f in os.listdir(orig_dir) if f.endswith('.JPG')])

    # sort only images with labels and labels attached to images

    image_labelled = []

    for i in range(len(file_bbox)):
        root_name_bbx = file_bbox[i].split('.')[0]
        for image in images:
            if image.split('.')[0] == root_name_bbx:
                image_labelled.append(image)

    image_labelled = sorted(image_labelled)

    labels_with_images = []

    for i in range(len(image_labelled)):
        root_name_image = image_labelled[i].split('.')[0]
        for label in file_bbox:
            if label.split('.')[0] == root_name_image:
                labels_with_images.append(label)

    labels_with_images = sorted(labels_with_images)

    bbox = load_bbox(labels_with_images, orig_dir)

    merged_bbox_list = []

    for i in range(len(bbox)):
        for j in range(len(bbox[i])):
            bbox[i][j].insert(0, images[i][0:-4])
        merged_bbox_list = merged_bbox_list + bbox[i]

    ground_truth = merged_bbox_list

    # loop through images with detection
    n = 0

    gt_df = pd.DataFrame(ground_truth, columns=('root_image', 'obj_class', 'x', 'y', 'w', 'h'))


    gt_df['obj_class'] = gt_df.obj_class.astype(str)
    gt_df['super_class'] = 'other'

    super_classes = []
    sub_classes = []

    with open(names_convert) as f:
        labels = [line.rstrip('\n') for line in f]

    for label in labels :
        superclass = label.split(' ')[1]
        subclass = label.split(' ')[0]
        super_classes.append(superclass)
        sub_classes.append(subclass)


    dir_name = outdir + '/' + super_class
    if not os.path.exists(dir_name):
        os.makedirs(dir_name)


    for row in gt_df.iterrows():
        index, data = row
        for i in range(len(sub_classes)):
            if (row[1].obj_class == str(float(i))):
                gt_df.at[index, 'obj_class'] = sub_classes[i]
                gt_df.at[index, 'super_class'] = super_classes[i]


    gt_df = gt_df[gt_df['super_class'] == super_class]


    # # print(len(gt_df[gt_df.obj_class == 'Pheidole_radoszkowskii_minor']))
    # pheidole_df = gt_df[gt_df.obj_class == 'Pheidole_radoszkowskii_minor']
    # non_pheidole = gt_df[gt_df.obj_class != 'Pheidole_radoszkowskii_minor']
    # n_solenopsis = len(gt_df[gt_df.obj_class == 'Solenopsis_geminata_minor'])
    # n_solenopsis_plus =  int(class_balance * n_solenopsis)
    
    # if len(pheidole_df)> n_solenopsis_plus:

    #     print('balance: n_pheidole = %d n_solenopsis' % class_balance)
    #     pheidole_sample = pheidole_df.sample(n = n_solenopsis_plus)
    #     # print(pheidole_sample)
    #     # print(len(pheidole_sample))

    #     frames = [non_pheidole, pheidole_sample]
    #     result = pd.concat(frames).reset_index(drop=True)
    #     gt_df = result
    #     # print('\n_______________________')
    #     # print(gt_df)
    
    # else :
    #     print('no balance')
    #     pass


    classes_sorted = sorted(gt_df.obj_class.unique())

    n_classes = len(classes_sorted)

    n_iterations = 0 # counting iteration for regular pause


    for image in gt_df.root_image.unique():

        print(image)

        image_path = image +'.JPG'

        image_path_tot = os.path.join(orig_dir, image_path)

        image0 = cv2.imread(image_path_tot, 1)  # color

        win_h, win_w = image0.shape[:2]
        nH = image0.shape[0]
        nW = image0.shape[1]

        dt_df_img = gt_df[gt_df['root_image'] == image]

        for row in dt_df_img.iterrows():

            n += 1

            x = int(row[1].x * nW)
            y = int(row[1].y * nH)
            w = int(row[1].w * nW)
            h = int(row[1].h * nH)

            half_w = int(w / 2)
            half_h = int(h / 2)

            # get cropped image at location
            window_c = image0[(y - half_h):(y + half_h), (x - half_w):(x + half_w)]

            outname = str(n)+'_'+str(row[1].obj_class)+'.png'

            out_dir_tot = outdir + '/' + row[1].super_class + '/'+mode+'/' + row[1].obj_class

            if not os.path.exists(out_dir_tot):
                os.makedirs(out_dir_tot)

            outpath = os.path.join(out_dir_tot, outname)

            # print(window_c.shape[1])
            # print(window_c.shape[0])
            if add_size :
                
                if (w > classifier_input_width):
                    w = classifier_input_width
                if (h > classifier_input_width):
                    h = classifier_input_width
                
                size_abs = w * h
                size_rel = float(float(size_abs)/float(classifier_input_width * classifier_input_width))
                pixel_value = 2 * int(size_rel * 255)
                if pixel_value > 255:
                    pixel_value = 255

                bg_img = window_c
                cv2.rectangle(bg_img,(0,0),(10,10),(pixel_value,0,0),-1)

                cv2.imwrite(outpath, bg_img)

            else :    
                cv2.imwrite(outpath, window_c)

        # make a pause every 100 loops
        n_iterations += 1
        if n_iterations % 100 == 0:
            print('resting...')
            time.sleep(60)

    return n_classes



def crop_im_test_imagewise(row, test_images_list, orig_dir = 'data/test/original', 
 add_bg=False, classifier_input_width = 416):

    index, data = row

    image = data.root_image

    image_path = image + '.JPG'

    if image_path in test_images_list:

        image_path_tot = os.path.join(orig_dir, image_path)

        image0 = cv2.imread(image_path_tot, 1)  # color

        win_h, win_w = image0.shape[:2]
        nH = image0.shape[0]
        nW = image0.shape[1]

        x = int(row[1].x * nW)
        y = int(row[1].y * nH)
        w = int(row[1].w * nW)
        h = int(row[1].h * nH)

        half_w = int(w / 2)
        half_h = int(h / 2)
        # print(half_h,half_w)
        # print(y - half_h,y + half_h,x - half_w,x + half_w)

        if(y - half_h < 0):
            half_h = y
        if(y + half_h > nH):
            half_h = nH - y
        if(x - half_w < 0):
            half_w = x
        if(x + half_w > nW):
            half_w = nW - x
        

        # get cropped image at location
        window_c = image0[(y - half_h):(y + half_h), (x - half_w):(x + half_w)]

        im_returned = window_c

        # print(window_c.shape[1])
        # print(window_c.shape[0])
        
        if add_bg:
            
            # add blank bg
            #ensures criooed image isnt bigger than canevas
            

            if h > classifier_input_width :
                window_c = cv2.resize(window_c, (w, classifier_input_width))
                half_h = int(classifier_input_width/2)
                h = classifier_input_width
                print('largeur')
                print(window_c.shape[1])
                print(window_c.shape[0])


            if w > classifier_input_width :
                window_c = cv2.resize(window_c, (classifier_input_width,h))
                half_w = int(classifier_input_width / 2)
                print('on est là')
                # cv2.imshow('image', window_c)
                # cv2.waitKey(0)
                # cv2.destroyAllWindows()


            # create canevas
            bg_img = np.zeros((classifier_input_width,classifier_input_width,3), np.uint8)
            # bg_img[:,0:classifier_input_width] = (228,201,154)
            bg_img[:, 0:classifier_input_width] = (154, 201, 228)
            x_offset=int((classifier_input_width/2) - half_w)
            y_offset = int((classifier_input_width/2) - half_h)
            bg_img[y_offset:y_offset+window_c.shape[0], x_offset:x_offset+window_c.shape[1]] = window_c

            im_returned = bg_img
        
        else:

            im_returned = window_c
            
            

    else:
        print("Image %s not found" % image_path)
    # return im_returned

    # A régler : gérer l'absence possible d'image à retourner
    return im_returned



def crop_im_test_imagewise_DEBUG(row, test_images_list, orig_dir = 'data/test/original', 
 add_bg=False, classifier_input_width = 416):

    index, data = row

    image = data.root_image

    image_path = image + '.JPG'

    if image_path in test_images_list:

        image_path_tot = os.path.join(orig_dir, image_path)

        image0 = cv2.imread(image_path_tot, 1)  # color

        win_h, win_w = image0.shape[:2]
        nH = image0.shape[0]
        nW = image0.shape[1]

        x = int(row[1].x * nW)
        y = int(row[1].y * nH)
        w = int(row[1].w * nW)
        h = int(row[1].h * nH)

        half_w = int(w / 2)
        half_h = int(h / 2)
        # print(half_h,half_w)
        # print(y - half_h,y + half_h,x - half_w,x + half_w)

        if(y - half_h < 0):
            half_h = y
        if(y + half_h > nH):
            half_h = nH - y
        if(x - half_w < 0):
            half_w = x
        if(x + half_w > nW):
            half_w = nW - x
        

        # get cropped image at location
        window_c = image0[(y - half_h):(y + half_h), (x - half_w):(x + half_w)]

        im_returned = window_c

        # print(window_c.shape[1])
        # print(window_c.shape[0])
        
        if add_bg:
            
            # add blank bg
            #ensures criooed image isnt bigger than canevas
            

            if h > classifier_input_width :
                window_c = cv2.resize(window_c, (w, classifier_input_width))
                half_h = int(classifier_input_width/2)
                h = classifier_input_width
                print('largeur')
                print(window_c.shape[1])
                print(window_c.shape[0])


            if w > classifier_input_width :
                window_c = cv2.resize(window_c, (classifier_input_width,h))
                half_w = int(classifier_input_width / 2)
                print('on est là')
                # cv2.imshow('image', window_c)
                # cv2.waitKey(0)
                # cv2.destroyAllWindows()


            # create canevas
            bg_img = np.zeros((classifier_input_width,classifier_input_width,3), np.uint8)
            # bg_img[:,0:classifier_input_width] = (228,201,154)
            bg_img[:, 0:classifier_input_width] = (154, 201, 228)
            x_offset=int((classifier_input_width/2) - half_w)
            y_offset = int((classifier_input_width/2) - half_h)
            bg_img[y_offset:y_offset+window_c.shape[0], x_offset:x_offset+window_c.shape[1]] = window_c

            im_returned = bg_img
        
        else:

            im_returned = window_c
            # outpath = 'data/test/cocci_extract/OPENCV-' + str(index) + '.png'
            # cv2.imwrite(outpath, window_c)


    else:
        print("Image %s not found" % image_path)
    # return im_returned

    # A régler : gérer l'absence possible d'image à retourner
    return im_returned








# def crop_im_test_size(detection_df, test_images_list, orig_dir = 'test_images',
#  outdir = 'test_temp/objects', outname='refined_detection_class', 
#  add_size=True, classifier_input_width = 416):

#     dt_df = detection_df

#     for row in dt_df.iterrows():

#         index, data = row

#         image = data.root_image

#         image_path = image + '.JPG'

#         if image_path in test_images_list:

#             image_path_tot = os.path.join(orig_dir, image_path)

#             image0 = cv2.imread(image_path_tot, 1)  # color

#             win_h, win_w = image0.shape[:2]
#             nH = image0.shape[0]
#             nW = image0.shape[1]

#             x = int(row[1].x * nW)
#             y = int(row[1].y * nH)
#             w = int(row[1].w * nW)
#             h = int(row[1].h * nH)

#             half_w = int(w / 2)
#             half_h = int(h / 2)
#             # print(half_h,half_w)
#             # print(y - half_h,y + half_h,x - half_w,x + half_w)

#             if(y - half_h < 0):
#                 half_h = y
#             if(y + half_h > nH):
#                 half_h = nH - y
#             if(x - half_w < 0):
#                 half_w = x
#             if(x + half_w > nW):
#                 half_w = nW - x
            

#             # get cropped image at location
#             window_c = image0[(y - half_h):(y + half_h), (x - half_w):(x + half_w)]

#             outname = str(index)+'-'+str(data.obj_class)+'.png'

#             outpath = os.path.join(outdir, outname)

#             # print(window_c.shape[1])
#             # print(window_c.shape[0])
            
#             if add_size :
                
#                 if (w > classifier_input_width):
#                     w = classifier_input_width
#                 if (h > classifier_input_width):
#                     h = classifier_input_width
                
#                 size_abs = w * h
#                 size_rel = float(float(size_abs)/float(classifier_input_width * classifier_input_width))
#                 pixel_value = 2 * int(size_rel * 255)
#                 if pixel_value > 255:
#                     pixel_value = 255

#                 bg_img = window_c
#                 cv2.rectangle(bg_img,(0,0),(10,10),(pixel_value,0,0),-1)

#                 cv2.imwrite(outpath, bg_img)

#             else :    
#                 cv2.imwrite(outpath, window_c)
            

#         # else:
#         #     print("Image %s not found" % image_path)
#     return





def crop_multi(detection_csv='output/refined_detections.csv', test_im_dir='data/test/original', 
out_dir = 'data/test/objects', add_size = False, classifier_input_width = 416):

    # read detection csv
    dt_df = pd.read_csv(detection_csv)

    # loop through images with detection
    n = 0
    test_images_list = [f for f in os.listdir(test_im_dir) if (f.endswith('.jpg') or f.endswith('.JPG'))]

    cpu_num = int(multiprocessing.cpu_count())
    
    chunked_list = chunkIt(test_images_list, cpu_num)

    manager = multiprocessing.Manager()
    return_dict = manager.dict()

    procs = []

    proc_num=0

    for chunk in chunked_list:
        
        proc_num +=1
        # creating processes 
        proc = multiprocessing.Process(target=crop_im_test_size, args=(dt_df,chunk, test_im_dir, out_dir, add_size, classifier_input_width)) 
        procs.append(proc)
        proc.start()
  
    # starting processes
    for proc in procs:
        
        proc.join() 

    # all processes finished 
    print("crop done!") 


#crop_im_train('test_images','test_classifier')