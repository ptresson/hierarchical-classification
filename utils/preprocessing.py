# -*- coding: utf-8 -*-
"""
cuts original images into slices and recalcultes labels of images containing enough of an object
"""

from __future__ import print_function
import os
import cv2
import time
import utils.Calc_IoU
import shutil
from utils.utils import load_bbox

def slice_train_valid(orig_dir, slice_dir, sliceHeight=416, sliceWidth=416, zero_frac_thresh=0.2, overlap=0.2, objectness_threshold = 0.4, slice_sep='|', out_ext='.png', verbose=False) :

    def slice_tot(image_path, bbox_path, out_name, outdir):
        '''Slice large satellite image into smaller pieces,
        ignore slices with a percentage null greater then zero_frac_thresh
        Assume three bands.
        if out_ext == '', use input file extension for saving'''
        image_path_tot = os.path.join(orig_dir,image_path)

        image0 = cv2.imread(image_path_tot, 1)  # color
        if len(out_ext) == 0:
            ext = '.' + image_path.split('.')[-1]
        else:
            ext = out_ext

        win_h, win_w = image0.shape[:2]
        nH = image0.shape[0]
        nW = image0.shape[1]

        # if slice sizes are large than image, pad the edges
        pad = 0
        if sliceHeight > win_h:
            pad = sliceHeight - win_h
        if sliceWidth > win_w:
            pad = max(pad, sliceWidth - win_w)
        # pad the edge of the image with black pixels
        if pad > 0:
            border_color = (0,0,0)
            image0 = cv2.copyMakeBorder(image0, pad, pad, pad, pad,
                                     cv2.BORDER_CONSTANT, value=border_color)

        win_size = sliceHeight*sliceWidth


        n_ims = 0
        n_ims_nonull = 0
        dx = int((1. - overlap) * sliceWidth)
        dy = int((1. - overlap) * sliceHeight)

        # bbox
        m = len(bbox_path)
        data_txt_abs = bbox_path

        for j in range(m):
            data_txt_abs[j][0] = int(bbox_path[j][0])  # classe
            data_txt_abs[j][1] = int(bbox_path[j][1] * nW)  # x
            data_txt_abs[j][2] = int(bbox_path[j][2] * nH)  # y
            data_txt_abs[j][3] = int(bbox_path[j][3] * nW)  # w
            data_txt_abs[j][4] = int(bbox_path[j][4] * nH)  # h








        for y0 in range(0, image0.shape[0], dy):#sliceHeight):
            for x0 in range(0, image0.shape[1], dx):#sliceWidth):
                n_ims += 1

                # if (n_ims % 100) == 0:
                #     print (n_ims)

                # make sure we don't have a tiny image on the edge
                if y0+sliceHeight > image0.shape[0]:
                    y = image0.shape[0] - sliceHeight
                else:
                    y = y0
                if x0+sliceWidth > image0.shape[1]:
                    x = image0.shape[1] - sliceWidth
                else:
                    x = x0

                # extract image
                window_c = image0[y:y + sliceHeight, x:x + sliceWidth]
                # get black and white image
                window = cv2.cvtColor(window_c, cv2.COLOR_BGR2GRAY)

                # find threshold that's not black
                # https://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_imgproc/py_thresholding/py_thresholding.html?highlight=threshold
                ret,thresh1 = cv2.threshold(window, 2, 255, cv2.THRESH_BINARY)
                non_zero_counts = cv2.countNonZero(thresh1)
                zero_counts = win_size - non_zero_counts
                zero_frac = float(zero_counts) / win_size
                #print "zero_frac", zero_fra
                # skip if image is mostly empty
                if zero_frac >= zero_frac_thresh:
                    if verbose:
                        print ("Zero frac too high at:", zero_frac)
                    continue
                # else save
                else:
                    #outpath = os.path.join(outdir, out_name + \
                    #'|' + str(y) + '_' + str(x) + '_' + str(sliceHeight) + '_' + str(sliceWidth) +\
                    #'_' + str(pad) + ext)
                    outpath = os.path.join(outdir, out_name + \
                    slice_sep + str(y) + '_' + str(x) + '_' + str(sliceHeight) + '_' + str(sliceWidth) +\
                    '_' + str(pad) + '_' + str(win_w) + '_' + str(win_h) + ext)

                    #outpath = os.path.join(outdir, 'slice_' + out_name + \
                    #'_' + str(y) + '_' + str(x) + '_' + str(sliceHeight) + '_' + str(sliceWidth) +\
                    #'_' + str(pad) + '.jpg')
                    if verbose:
                        print ("outpath:", outpath)
                    cv2.imwrite(outpath, window_c)
                    n_ims_nonull += 1



                ##################### slicing bboxes #############################################

                x_min_slice = x
                x_max_slice = x + sliceWidth
                y_min_slice = y
                y_max_slice = y + sliceHeight
                x_slice_abs = x + int(0.5 * sliceWidth)
                y_slice_abs = y + int(0.5 * sliceHeight)

                txt_file_name = ("%s.txt" % (outpath[0:-4]))

                text_file = open(txt_file_name, "w")


                m = len(data_txt[i])
                for j in range(m):

                    X_obj_abs = data_txt_abs[j][1]
                    Y_obj_abs = data_txt_abs[j][2]
                    W_obj_abs = data_txt_abs[j][3]
                    H_obj_abs = data_txt_abs[j][4]
                    object_area_abs = W_obj_abs * H_obj_abs

                    # X_min_obj_abs = int(X_obj_abs - 0.5 * W_obj_abs)
                    # X_max_obj_abs = int(X_obj_abs + 0.5 * W_obj_abs)
                    # Y_min_obj_abs = int(Y_obj_abs - 0.5 * H_obj_abs)
                    # Y_max_obj_abs = int(Y_obj_abs + 0.5 * H_obj_abs)


                    Inter = int(utils.Calc_IoU.calc_Inter(X_obj_abs, x_slice_abs, Y_obj_abs, y_slice_abs, W_obj_abs, sliceWidth, H_obj_abs, sliceHeight))
                    object_covering = float(Inter/object_area_abs)

                    window_covering = float(Inter/win_size)

                    centroid_inside_window = ((x_min_slice <= X_obj_abs <= x_max_slice) and (y_min_slice <= Y_obj_abs <= y_max_slice))

                    if (object_covering > objectness_threshold or window_covering > 0.5 or centroid_inside_window):
                        x_obj_slice_temp = float((X_obj_abs - x_min_slice) / sliceWidth)
                        y_obj_slice_temp = float((Y_obj_abs - y_min_slice) / sliceHeight)
                        w_obj_slice_temp = float(W_obj_abs / sliceWidth)
                        h_obj_slice_temp = float(H_obj_abs / sliceHeight)
                        # print(w_obj_slice_temp, h_obj_slice_temp)

                        x_min_obj_slice = float(x_obj_slice_temp - 0.5 * w_obj_slice_temp)
                        x_max_obj_slice = float(x_obj_slice_temp + 0.5 * w_obj_slice_temp)
                        y_min_obj_slice = float(y_obj_slice_temp - 0.5 * h_obj_slice_temp)
                        y_max_obj_slice = float(y_obj_slice_temp + 0.5 * h_obj_slice_temp)
                        # print("xmin xmax ymin ymax")
                        # print(x_min_obj_slice, x_max_obj_slice, y_min_obj_slice, y_max_obj_slice)

                        if (x_min_obj_slice < 0):
                            x_min_obj_slice = 0
                        if (y_min_obj_slice < 0):
                            y_min_obj_slice = 0
                        if (x_max_obj_slice > 1):
                            x_max_obj_slice = 1
                        if (y_max_obj_slice > 1):
                            y_max_obj_slice = 1

                        w_obj_slice = float(x_max_obj_slice - x_min_obj_slice)
                        h_obj_slice = float(y_max_obj_slice - y_min_obj_slice)
                        x_obj_slice = float(x_min_obj_slice + 0.5 * w_obj_slice)
                        y_obj_slice = float(y_min_obj_slice + 0.5 * h_obj_slice)
                        #print(x_obj_slice, y_obj_slice, w_obj_slice, h_obj_slice)

                        text_file.write("%d %4f %4f %4f %4f\n" % (
                            data_txt_abs[j][0], x_obj_slice, y_obj_slice, w_obj_slice,
                            h_obj_slice))

                text_file.close()

                # remove file if empty
                # print(os.path.getsize(txt_file_name))

                if os.stat(txt_file_name).st_size == 0:
                    os.remove(txt_file_name)


        return


    data_image = sorted([f for f in os.listdir(orig_dir) if (f.endswith('.JPG') or f.endswith('.jpg'))])
    n = len(data_image)

    # print(origine_slice)
    files_bbox = sorted([f for f in os.listdir(orig_dir) if f.endswith('.txt')])
    # print(files_bbox)
    data_txt = load_bbox(files_bbox, orig_dir)

    for i in range(n):
        print(str(data_image[i][0:-4]) + ' (' + str(i+1) + '/' + str(n) + ')')
        slice_tot(data_image[i],data_txt[i] ,data_image[i][0:-4], slice_dir)

###########################   slice for test and regular detection  ###########################################

# slice test only slices images
def slice_test(orig_dir, slice_dir, sliceHeight=416, sliceWidth=416, zero_frac_thresh=0.2, overlap=0.2, slice_sep='|', out_ext='.png', verbose=False) :

    def slice_im(image_path, out_name, outdir):
        '''Slice large satellite image into smaller pieces,
        ignore slices with a percentage null greater then zero_frac_thresh
        Assume three bands.
        if out_ext == '', use input file extension for saving'''
        image_path_tot = os.path.join(orig_dir,image_path)

        image0 = cv2.imread(image_path_tot, 1)  # color
        if len(out_ext) == 0:
            ext = '.' + image_path.split('.')[-1]
        else:
            ext = out_ext

        win_h, win_w = image0.shape[:2]
        # nH = image0.shape[0]
        # nW = image0.shape[1]

        # if slice sizes are large than image, pad the edges
        pad = 0
        if sliceHeight > win_h:
            pad = sliceHeight - win_h
        if sliceWidth > win_w:
            pad = max(pad, sliceWidth - win_w)
        # pad the edge of the image with black pixels
        if pad > 0:
            border_color = (0,0,0)
            image0 = cv2.copyMakeBorder(image0, pad, pad, pad, pad,
                                     cv2.BORDER_CONSTANT, value=border_color)

        win_size = sliceHeight*sliceWidth

        n_ims = 0
        n_ims_nonull = 0
        dx = int((1. - overlap) * sliceWidth)
        dy = int((1. - overlap) * sliceHeight)

        for y0 in range(0, image0.shape[0], dy):#sliceHeight):
            for x0 in range(0, image0.shape[1], dx):#sliceWidth):
                n_ims += 1

                # if (n_ims % 100) == 0:
                #     print (n_ims)

                # make sure we don't have a tiny image on the edge
                if y0+sliceHeight > image0.shape[0]:
                    y = image0.shape[0] - sliceHeight
                else:
                    y = y0
                if x0+sliceWidth > image0.shape[1]:
                    x = image0.shape[1] - sliceWidth
                else:
                    x = x0

                # extract image
                window_c = image0[y:y + sliceHeight, x:x + sliceWidth]
                # get black and white image
                window = cv2.cvtColor(window_c, cv2.COLOR_BGR2GRAY)

                # find threshold that's not black
                # https://opencv-python-tutroals.readthedocs.org/en/latest/py_tutorials/py_imgproc/py_thresholding/py_thresholding.html?highlight=threshold
                ret,thresh1 = cv2.threshold(window, 2, 255, cv2.THRESH_BINARY)
                non_zero_counts = cv2.countNonZero(thresh1)
                zero_counts = win_size - non_zero_counts
                zero_frac = float(zero_counts) / win_size
                #print "zero_frac", zero_fra
                # skip if image is mostly empty
                if zero_frac >= zero_frac_thresh:
                    if verbose:
                        print ("Zero frac too high at:", zero_frac)
                    continue
                # else save
                else:
                    #outpath = os.path.join(outdir, out_name + \
                    #'|' + str(y) + '_' + str(x) + '_' + str(sliceHeight) + '_' + str(sliceWidth) +\
                    #'_' + str(pad) + ext)
                    outpath = os.path.join(outdir, out_name + \
                    slice_sep + str(y) + '_' + str(x) + '_' + str(sliceHeight) + '_' + str(sliceWidth) +\
                    '_' + str(pad) + '_' + str(win_w) + '_' + str(win_h) + ext)

                    #outpath = os.path.join(outdir, 'slice_' + out_name + \
                    #'_' + str(y) + '_' + str(x) + '_' + str(sliceHeight) + '_' + str(sliceWidth) +\
                    #'_' + str(pad) + '.jpg')
                    if verbose:
                        print ("outpath:", outpath)
                    cv2.imwrite(outpath, window_c)
                    n_ims_nonull += 1


        return


    data_image = sorted([f for f in os.listdir(orig_dir) if (f.endswith('.JPG') or f.endswith('.jpg'))])
    n = len(data_image)

    # print(origine_slice)
    # files_bbox = sorted([f for f in os.listdir(orig_dir) if f.endswith('.txt')])
    # print(files_bbox)
    # data_txt = load_bbox(files_bbox, orig_dir)

    for i in range(n):
        print(str(data_image[i][0:-4]) + ' (' + str(i+1) + '/' + str(n) + ')')
        slice_im(data_image[i], data_image[i][0:-4], slice_dir)




def sort_empty_slices(current_dir):

    label_dir = os.path.join(current_dir,'labels')
    images_dir = os.path.join(current_dir,'images')
    background_dir = os.path.join(current_dir,'background_only')

    # Prendre les fichiers du dossiers en .JPG

    images_files = [f for f in os.listdir(current_dir) if f.endswith('.png')]
    labels_files = [f for f in os.listdir(current_dir) if f.endswith('.txt')]
    # print(images_files)
    # print(labels_files)

    images_without_ext = []
    for image_name in images_files:
        images_without_ext.append(image_name.split('.')[0])

    labels_without_ext = []
    for label_name in labels_files:
        labels_without_ext.append(label_name.split('.')[0])

    images_with_labels = set(images_without_ext).intersection(labels_without_ext)

    images_with_labels_list = list(images_with_labels)
    # print(images_with_labels_list)

    images_with_labels_ext = [name + '.png' for name in images_with_labels_list]


    # print(images_with_labels_ext)

    for image in images_files:

        source = os.path.join(current_dir, image)
        if image in images_with_labels_ext:
            dest = os.path.join(images_dir, image)
            
        else:
            dest = os.path.join(background_dir, image)
        
        shutil.move(source, dest)
    
    for label in labels_files:
        source = os.path.join(current_dir, label)
        dest = os.path.join(label_dir, label)
        shutil.move(source, dest)


    return
    

def update_data_file(data_file_path='config/train.data', train_list_file='data/train.txt',
 valid_list_file='data/valid.txt', name_file='data/classes.names'):

    """
    edit data file needed for darknet
    this file tell darknet where to get the different data or information
    so it has to be updated

    """
    num_classes = sum(1 for line in open(name_file))

    data_file = open(data_file_path, 'w')
    data_file.write('classes= %d\n' % num_classes)
    data_file.write('train= %s\n' % train_list_file)
    data_file.write('valid= %s\n' % valid_list_file)
    data_file.write('names= %s\n' % name_file)
    # data_file.write('backup = %s/backup\n' % train_tmp_dir)  # where the weights files will be stored
    data_file.close()