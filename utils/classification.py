from __future__ import print_function
import os
import cv2
import pandas as pd
import numpy as np
from utils.utils import load_bbox, list_files, sort_classes_into_directories
from utils.extract_objects import crop_im_test_imagewise
import time
from PIL import Image
# from utils.Calc_IoU import calc_IOU

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.transforms as transforms
from torch.autograd import Variable


def image_loader(loader, image_name):
    image = Image.fromarray(image_name.astype('uint8'), 'RGB')
    image = loader(image).float()
    image = torch.tensor(image, requires_grad=True)
    return image

data_transforms = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(224),
    transforms.ToTensor()
])

test_transforms = transforms.Compose([transforms.Resize((32,32)),
                                      transforms.ToTensor(),
                                     ])


# def predict_image(image):
#     image_tensor = test_transforms(image).float()
#     image_tensor = image_tensor.unsqueeze_(0)
#     input = Variable(image_tensor)
#     input = input.to(device)
#     output = net(input)
#     index = output.data.cpu().numpy().argmax()
#     return index


def Classification(cfg_dir = 'config', names_convert_file = 'config/insects_convert.names',
 refined_detection_file='output/refined_detections.csv', confidence_threshold = 0.2, 
 trained_model_path = 'cifar_net.pth'):

    t0 = time.time()

    # create new refined detection
    refined_df_original = pd.read_csv(refined_detection_file)
    refined_df = refined_df_original.copy(deep=True)

    # add column obj_class_detailled
    refined_df['sub_class'] = 'unknown'
    refined_df['check_status'] = 'valid'
    refined_df['index'] = 'NA'
    refined_df['gt_class'] = 'NA'

    # add column classification_confidence
    refined_df['classif_confidence'] = 0.0

    test_image_list = [f for f in os.listdir('data/test/original') if f.endswith('.JPG') or f.endswith('.jpg')]

    #load model
    classifier_data_dir='data/train/classifier_data/ant'

    sort_classes_into_directories(classifier_data_dir)

    device = torch.device("cuda" if torch.cuda.is_available() 
                                    else "cpu")
    
    classes = os.listdir(classifier_data_dir)
    num_classes = len(classes)

    class Net(nn.Module):
        def __init__(self):
            super(Net, self).__init__()
            self.conv1 = nn.Conv2d(3, 6, 5)
            self.pool = nn.MaxPool2d(2, 2)
            self.conv2 = nn.Conv2d(6, 16, 5)
            self.conv3 = nn.Conv2d(6, 16, 5)
            self.conv4 = nn.Conv2d(6, 16, 5)
            self.fc1 = nn.Linear(16 * 5 * 5, 120)
            self.fc2 = nn.Linear(120, 84)
            self.fc3 = nn.Linear(84, 10)

        def forward(self, x):
            x = self.pool(F.relu(self.conv1(x)))
            x = self.pool(F.relu(self.conv2(x)))
            x = x.view(-1, 16 * 5 * 5)
            x = F.relu(self.fc1(x))
            x = F.relu(self.fc2(x))
            x = self.fc3(x)
            return x


    net = Net()
    net.fc3 = nn.Linear(84,num_classes)
    net.load_state_dict(torch.load(trained_model_path))
    net.to(device)

    def predict_image(image):
        image_tensor = test_transforms(image).float()
        image_tensor = image_tensor.unsqueeze_(0)
        input = Variable(image_tensor)
        input = input.to(device)
        output = net(input)
        index = output.data.cpu().numpy().argmax()
        return index
        
    # select ants

    for row in refined_df.iterrows():

        index, data = row

        if data.obj_class == 'fourmi_a_verif':

            # extract
            image = crop_im_test_imagewise(row, test_image_list)
            image = Image.fromarray(image)                        
            # resize

            # classify
            class_int = predict_image(image)

            print(class_int)

            subclass = classes[class_int]
            
            # replace subclass
            refined_df.at[index, 'sub_class'] = subclass


    refined_df.to_csv('post_classif.csv', index=False)  
        




    # for ant in list : classif
    # get results

    # result_list = read_classification('results_classifier.txt')  # read results

    # print(result_list)

    # update refined_detections at indexes

    # for classification in result_list:
    #     index = classification[0]
    #     obj_class = classification[1]
    #     confidence = classification[2]
    #     refined_df.at[index, 'sub_class'] = obj_class
    #     refined_df.at[index, 'index'] = index

    #     if confidence < confidence_threshold:
    #         refined_df.at[index, 'check_status'] = 'to_check'

    #     refined_df.at[index, 'classif_confidence'] = confidence

    # else:

    #     # print('pouet')

    #     for row in refined_df.iterrows():

    #         index, data = row

    #         if data.obj_class != 'ant':

    #             refined_df.at[index, 'sub_class'] = data.obj_class
        
    # #
    # #
    # #
    # #
    # #
    # #### add column with GT class
    
    # test_im_dir = 'test_images'

    # file_bbox = sorted([f for f in os.listdir(test_im_dir) if f.endswith('.txt')])
    # images = sorted([f for f in os.listdir(test_im_dir) if f.endswith('.JPG')])

    # # sort only images with labels and labels attached to images

    # image_labelled = []

    # for i in range(len(file_bbox)):
    #     root_name_bbx = file_bbox[i].split('.')[0]
    #     for image in images:
    #         if image.split('.')[0] == root_name_bbx:
    #             image_labelled.append(image)

    # image_labelled = sorted(image_labelled)

    # labels_with_images = []

    # for i in range(len(image_labelled)):
    #     root_name_image = image_labelled[i].split('.')[0]
    #     for label in file_bbox:
    #         if label.split('.')[0] == root_name_image:
    #             labels_with_images.append(label)

    # labels_with_images = sorted(labels_with_images)

    # bbox = load_bbox(labels_with_images, test_im_dir)

    # merged_bbox_list = []

    # for i in range(len(bbox)):
    #     for j in range(len(bbox[i])):
    #         bbox[i][j].insert(0, images[i][0:-4])
    #     merged_bbox_list = merged_bbox_list + bbox[i]

    # ground_truth = merged_bbox_list

    # # gt_df stores GT information
    # # dt_df stores detection information
    # gt_df = pd.DataFrame(ground_truth, columns=('root_image','sub_class', 'x', 'y', 'w', 'h'))
    

    # for image in refined_df.root_image.unique():

    #     print(image)

    #     dt_df_img = refined_df[refined_df['root_image'] == image]
    #     gt_df_img = gt_df[gt_df['root_image'] == image]

    #     for rowA in gt_df_img.iterrows():  #reference each corresponding gt_object

    #         xA = rowA[1].x
    #         yA = rowA[1].y
    #         wA = rowA[1].w
    #         hA = rowA[1].h
    #         gt_class = rowA[1].sub_class

    #         for rowB in dt_df_img.iterrows(): 

    #             xB = rowB[1].x
    #             yB = rowB[1].y
    #             wB = rowB[1].w
    #             hB = rowB[1].h

    #             IOU = calc_IOU(xA, xB, yA, yB, wA, wB, hA, hB)
    #             overlap_threshold = 0.5

    #             if IOU > overlap_threshold :  # i.e. if detection and GT overlap
    #                 indexB, dataB = rowB
    #                 refined_df.at[indexB, 'gt_class'] = gt_class








    # # print(refined_df)

    # # second round ?
    # # refined_df.to_csv('test_temp/refined_classification_precise.csv', index=False)
    # # to_check_df = refined_df[refined_df['check_status'] == 'to_check']
    # sure_df_tmp = refined_df[refined_df['check_status'] != 'to_check']
    # sure_df = sure_df_tmp[sure_df_tmp['sub_class'] != 'unknown']
    # sure_df.to_csv('test_temp/refined_classification_-to_check.csv', index=False)
    # # print(to_check_df)
    # refined_df.to_csv('test_temp/refined_classification.csv', index=False)

    # t1 = time.time()

    # duration = t1-t0

    # print('\nClassification time (s):')
    # print(duration)




    return

Classification()

    # classifier_data_dir='data/train/classifier_data/ant'

    # sort_classes_into_directories(classifier_data_dir)

    # transform = transforms.Compose(
    #     [transforms.Resize((32,32)),
    #         transforms.ToTensor()])

    # train_dataset = datasets.ImageFolder(classifier_data_dir, transform=transform)

    # trainloader = torch.utils.data.DataLoader(train_dataset, batch_size=2, num_workers=8)

    # device = torch.device("cuda" if torch.cuda.is_available() 
    #                                 else "cpu")

    # ########################################################################
    # # 2. Define a Convolutional Neural Network
    # # ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
    # # Copy the neural network from the Neural Networks section before and modify it to
    # # take 3-channel images (instead of 1-channel images as it was defined).

    # num_classes = len(os.listdir(classifier_data_dir))

    # class Net(nn.Module):
    #     def __init__(self):
    #         super(Net, self).__init__()
    #         self.conv1 = nn.Conv2d(3, 6, 5)
    #         self.pool = nn.MaxPool2d(2, 2)
    #         self.conv2 = nn.Conv2d(6, 16, 5)
    #         self.conv3 = nn.Conv2d(6, 16, 5)
    #         self.conv4 = nn.Conv2d(6, 16, 5)
    #         self.fc1 = nn.Linear(16 * 5 * 5, 120)
    #         self.fc2 = nn.Linear(120, 84)
    #         self.fc3 = nn.Linear(84, 10)

    #     def forward(self, x):
    #         x = self.pool(F.relu(self.conv1(x)))
    #         x = self.pool(F.relu(self.conv2(x)))
    #         x = x.view(-1, 16 * 5 * 5)
    #         x = F.relu(self.fc1(x))
    #         x = F.relu(self.fc2(x))
    #         x = self.fc3(x)
    #         return x


    # net = Net()
    # net.fc3 = nn.Linear(84,num_classes)

# net = Net()
# net.load_state_dict(torch.load(PATH))

# ########################################################################
# # Okay, now let us see what the neural network thinks these examples above are:

# outputs = net(images)

# ########################################################################
# # The outputs are energies for the 10 classes.
# # The higher the energy for a class, the more the network
# # thinks that the image is of the particular class.
# # So, let's get the index of the highest energy:
# _, predicted = torch.max(outputs, 1)

# print('Predicted: ', ' '.join('%5s' % classes[predicted[j]]
#                               for j in range(4)))