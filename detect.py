from __future__ import division

from models import *
from utils.utils import *
from utils.datasets import *

import os
import sys
import time
import datetime
import argparse

from PIL import Image

import torch
from torch.utils.data import DataLoader
from torchvision import datasets
from torch.autograd import Variable

import matplotlib.pyplot as plt
import matplotlib.patches as patches
from matplotlib.ticker import NullLocator

def detect(image_folder="data/test/sliced/images",
model_def="config/yolov3-custom.cfg",weights_path="checkpoints/yolov3_ckpt_best.pth",
class_path="data/classes.names", conf_thres= 0.8, nms_thres=0.4, batch_size=1, n_cpu=12, img_size=416, 
checkpoint_model="checkpoints/yolov3_ckpt_best.pth"):
    
    """
    Perform detection. Code based on https://github.com/eriklindernoren/PyTorch-YOLOv3
    Returns detection information as a list of dict.
    """

    n_slices = len(os.listdir(image_folder))

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    os.makedirs("output", exist_ok=True)

    # Set up model
    model = Darknet(model_def, img_size=img_size).to(device)

    if weights_path.endswith(".weights"):
        # Load darknet weights
        model.load_darknet_weights(weights_path)
    else:
        # Load checkpoint weights
        model.load_state_dict(torch.load(weights_path))

    model.eval()  # Set in evaluation mode

    dataloader = DataLoader(
        ImageFolder(image_folder, img_size=img_size),
        batch_size=batch_size,
        shuffle=False,
        num_workers=n_cpu,
    )

    classes = load_classes(class_path)  # Extracts class labels from file

    Tensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor

    imgs = []  # Stores image paths
    img_detections = []  # Stores detections for each image index

    print("\nPerforming object detection:")
    prev_time = time.time()
    for batch_i, (img_paths, input_imgs) in enumerate(dataloader):
        # Configure input
        input_imgs = Variable(input_imgs.type(Tensor))

        # Get detections
        with torch.no_grad():
            detections = model(input_imgs)
            detections = non_max_suppression(detections, conf_thres, nms_thres)

        # Log progress
        current_time = time.time()
        inference_time = datetime.timedelta(seconds=current_time - prev_time)
        prev_time = current_time
        print("\t+ Batch %d/%d, Inference Time: %s" % (batch_i, n_slices, inference_time))

        # Save image and detections
        imgs.extend(img_paths)
        img_detections.extend(detections)

    # Bounding-box colors
    cmap = plt.get_cmap("tab20b")
    colors = [cmap(i) for i in np.linspace(0, 1, 20)]

    print("\nSaving images:")

    # Save detections in list
    all_detections = []
    
    for img_i, (path, detections) in enumerate(zip(imgs, img_detections)):

        print("(%d) Image: '%s'" % (img_i, path))
        img = np.array(Image.open(path))

        # Draw bounding boxes and labels of detections
        if detections is not None:
            # Rescale boxes to original image
            detections = rescale_boxes(detections, img_size, img.shape[:2])
            unique_labels = detections[:, -1].cpu().unique()
            n_cls_preds = len(unique_labels)

            for x1, y1, x2, y2, conf, cls_conf, cls_pred in detections:

                print("\t+ Label: %s, Conf: %.5f" % (classes[int(cls_pred)], cls_conf.item()))

                detection_dict = {}

                box_class = classes[int(cls_pred)]
                box_conf = cls_conf.item()
                box_w = int(x2 - x1)
                box_h = int(y2 - y1)
                box_x = int((box_w / 2) + x1)
                box_y = int((box_h / 2) + y1)

                image_name = path.split('/')[-1]
                root_image = image_name.split('|')[0]
                coord_ext = image_name.split("|")[1]
                coord = coord_ext.split(".")[0]
                y_slice = int(coord.split("_")[0])
                x_slice = int(coord.split("_")[1])
                # SliceWidth = int(coord.split("_")[2])
                # SliceHeight = int(coord.split("_")[3])
                nW = int(coord.split("_")[5])
                nH = int(coord.split("_")[6])

                left_x = int(x1)
                top_y = int(y1)
                w_obj = box_w
                h_obj = box_h

                left_x_abs = left_x + x_slice
                top_y_abs = top_y + y_slice
                x_abs = left_x_abs + int(w_obj/2)
                y_abs = top_y_abs + int(h_obj/2)

                x_rel = float(x_abs/nW)
                y_rel = float(y_abs/nH)
                w_rel = float(w_obj/nW)
                h_rel = float(h_obj/nH)

                ## results as a list 
                coordinates = [root_image, box_class, box_conf, x_rel, y_rel, w_rel, h_rel]

                ## results as a dict
                # detection_dict['image'] = root_image
                # detection_dict['obj_class'] = box_class
                # detection_dict['confidence'] = box_conf
                # detection_dict['x'] = x_rel
                # detection_dict['y'] = y_rel
                # detection_dict['w'] = w_rel
                # detection_dict['h'] = h_rel

                # all_detections.append(detection_dict)
                all_detections.append(coordinates)

                print("\t x:%2f, y:%2f" %(box_x, box_y))
    
    return all_detections



    # Iterate through images and save plot of detections
    # for img_i, (path, detections) in enumerate(zip(imgs, img_detections)):

    #     print("(%d) Image: '%s'" % (img_i, path))

    #     # Create plot
    #     img = np.array(Image.open(path))
    #     plt.figure()
    #     fig, ax = plt.subplots(1)
    #     ax.imshow(img)

    #     # Draw bounding boxes and labels of detections
    #     if detections is not None:
    #         # Rescale boxes to original image
    #         detections = rescale_boxes(detections, img_size, img.shape[:2])
    #         unique_labels = detections[:, -1].cpu().unique()
    #         n_cls_preds = len(unique_labels)
    #         bbox_colors = random.sample(colors, n_cls_preds)
    #         for x1, y1, x2, y2, conf, cls_conf, cls_pred in detections:

    #             print("\t+ Label: %s, Conf: %.5f" % (classes[int(cls_pred)], cls_conf.item()))

    #             box_w = x2 - x1
    #             box_h = y2 - y1
    #             box_x = int((box_w / 2) + x1)
    #             box_y = int((box_h / 2) + y1)
    #             plt.scatter (box_x, box_y, 10)

    #             color = bbox_colors[int(np.where(unique_labels == int(cls_pred))[0])]
    #             # Create a Rectangle patch
    #             bbox = patches.Rectangle((x1, y1), box_w, box_h, linewidth=2, edgecolor=color, facecolor="none")
    #             # Add the bbox to the plot
    #             ax.add_patch(bbox)
    #             # Add label
    #             plt.text(
    #                 x1,
    #                 y1,
    #                 s=classes[int(cls_pred)],
    #                 color="white",
    #                 verticalalignment="top",
    #                 bbox={"color": color, "pad": 0},
    #             )
# 
        # # Save generated image with detections
        # plt.axis("off")
        # plt.gca().xaxis.set_major_locator(NullLocator())
        # plt.gca().yaxis.set_major_locator(NullLocator())
        # filename = path.split("/")[-1].split(".")[0]
        # plt.savefig(f"output/{filename}.png", bbox_inches="tight", pad_inches=0.0)
        # plt.close()


# detect()