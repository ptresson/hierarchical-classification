import os
import time
import subprocess
import pandas as pd

from utils.preprocessing import slice_train_valid, slice_test, sort_empty_slices, update_data_file
import utils.slice_PIL

from utils.postprocessing import multi_proc_manager
from utils.extract_objects import crop_multi, crop_im_train
# from utils.classification import Classification # depreciated, best to use classification_standalone.py

from utils.performances_analysis import get_detection_metrics


from utils.utils import clean_dir, list_files, get_last_file
from train import train_detector, train_classifier
from detect import detect

from utils.plot_bbox import plot_bbx

# use parameters

MODE = 'test_detector'
# MODE = 'train_classifier'
RESUME = False # true if 
DETECTION_CLASSES = 'detailled' # for flat detector, all classes at species level 
# DETECTION_CLASSES = 'superclasses' # for general detector, classes regrouped as super-classes
WEIGHTS_PATH = "checkpoints/yolov3_flat_detector.pth"
# WEIGHTS_PATH = "checkpoints/yolov3_general_detector.pth"

SUPER_CLASS= "cocco" # super-class for classifier training. here "cocco" for coccoidae

if __name__ == "__main__":

    ################################### TRAIN DETECTOR #########################################

    if MODE == 'train_detector':
        

        if RESUME : 

            print(get_last_file('checkpoints/*'))
            print('sure to use these weights ?')
            time.sleep(10)
            train_detector(pretrained_weights=get_last_file('checkpoints/*'),batch_size=4)

        else :

            ## update config file according to class number
            num_classes = sum(1 for line in open('data/classes.names'))
            
            if os.path.isfile('config/yolov3-custom.cfg'):
                os.remove('config/yolov3-custom.cfg')
            
            os.system('bash config/create_custom_model_remote_exec.sh %d' % num_classes)

            ### test images are processed as well for valdation

            # cleaning directories
            clean_dir('data/train/sliced/images')
            clean_dir('data/train/sliced/labels')
            clean_dir('data/train/sliced/background_only')

            clean_dir('data/test/sliced/images')
            clean_dir('data/test/sliced/labels')
            clean_dir('data/test/sliced/background_only')

            ## preprocessing : slicing and sorting images
            print('slicing...')
            if DETECTION_CLASSES == 'detailled':
                
                slice_train_valid('data/train/original_detailled','data/train/sliced')
                slice_train_valid('data/test/original_detailled','data/test/sliced')
            
            if DETECTION_CLASSES == 'superclasses':
                
                slice_train_valid('data/train/original','data/train/sliced')
                slice_train_valid('data/test/original','data/test/sliced')

            print('sorting empty slices...')
            sort_empty_slices('data/train/sliced')
            sort_empty_slices('data/test/sliced')

            print('listing...')
            list_files('data/train/sliced/images', '.png', 'data', 'train')
            list_files('data/test/sliced/images', '.png', 'data', 'valid')

            ## updating data file
            print("updating .data ...")
            update_data_file()

            print('preprocessing done !')

            ## actual training
            train_detector(batch_size=4)

    ################################### TEST DETECTOR #########################################

    if MODE == 'test_detector':

        # cleaning directories
        print('slicing sure ? (start in 10s)') # quick prompt if slicing is not needed beacause allready done. Just time to abord and comment out this part
        time.sleep(10)
        clean_dir('data/test/sliced/all_slices')

        if DETECTION_CLASSES == 'detailled':
            print('detailled (slicing)')
            print('sure ? (start in 10s)')
            time.sleep(10)
            slice_test('data/test/original_detailled','data/test/sliced/all_slices')
        else :
            print('general (slicing)')
            print('sure ? (start in 10s)')
            time.sleep(10)            
            slice_test('data/test/original','data/test/sliced/all_slices')

    
        ## update config file according to class number
        num_classes = sum(1 for line in open('data/classes.names'))
        
        if os.path.isfile('config/yolov3-custom.cfg'):
            os.remove('config/yolov3-custom.cfg')
        
        os.system('bash config/create_custom_model_remote_exec.sh %d' % num_classes)

        ## updating data file
        print("updating .data ...")
        update_data_file()
        
        ## actual detection. detections are stored into a list for refining
        result_list = detect(image_folder='data/test/sliced/all_slices', 
        weights_path=WEIGHTS_PATH)

        print('refining...')
        multi_proc_manager(result_list) # merging and refining step
        
        print('getting metrics...')

        if DETECTION_CLASSES == 'detailled':
            get_detection_metrics('data/test/original_detailled','output/refined_detections.csv','data/classes.names',edit_res=True)

        else:
            get_detection_metrics('data/test/original','output/refined_detections.csv','data/classes.names')


    ################################ TRAIN CLASSIFIER ############################

    if MODE == 'train_classifier':
        
        # update paths
        classifier_data_dir = 'data/train/classifier_data/' + SUPER_CLASS
        classifier_data_dir_train = 'data/train/classifier_data/' + SUPER_CLASS + '/train'
        
        # cleaning directories
        clean_dir(classifier_data_dir)

        # extracting objects for training
        crop_im_train(super_class = SUPER_CLASS)
        # and validation
        crop_im_train(super_class = SUPER_CLASS, orig_dir = 'data/test/original_detailled', mode='val')
        num_classes = len(os.listdir(classifier_data_dir_train))

        # train classifier
        train_classifier(data_dir=classifier_data_dir,
        model_name='squeezenet',num_epochs=30,num_classes=num_classes)


    ############################# BENCHMARKING ##########################"

    if MODE == "benchmark_classifiers":

        # update paths
        classifier_data_dir = 'data/train/classifier_data/' + SUPER_CLASS
        classifier_data_dir_train = 'data/train/classifier_data/' + SUPER_CLASS + '/train'
        # cleaning directories
        clean_dir(classifier_data_dir)

        # extracting objects for training
        crop_im_train(super_class = SUPER_CLASS)
        # and validation
        crop_im_train(super_class = SUPER_CLASS, orig_dir = 'data/test/original_detailled', mode='val')
        num_classes = len(os.listdir(classifier_data_dir_train))


        ## benchmarking
        result_dict = {}

        for classifier in ["resnet", "alexnet", "vgg", "squeezenet", "densenet"]:

            model, av_accuracy = train_classifier(data_dir=classifier_data_dir,
            model_name=classifier, num_epochs=30,num_classes=num_classes)
            result_dict[classifier] = av_accuracy
            time.sleep(240)

        print("\n================================\n")
        print(result_dict)






    ############################### TEST CLASSIFIER ###############################

    # if MODE == 'test_classifier':

        ## cleaning directories
        # clean_dir('data/train/sliced/images') 
        
        ## extracting objects
        ## test classifier
        # Classification() # depreciated, best to use classification_standalone.py

